import 'package:delivery_app/Screen/Notification/notification.dart';
import 'package:delivery_app/Screen/profile/profile.dart';
import 'package:delivery_app/renderdata/loading.dart';
import 'package:delivery_app/utils/g_widget.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:delivery_app/helper/stringExtension.dart';


import 'Screen/home/homeScreen.dart';
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

class Tap extends StatefulWidget {
  @override
  _TapState createState() => _TapState();
}
class _TapState extends State<Tap> {
  late final FirebaseMessaging _messaging;
  late int _totalNotifications;
  void registerNotification() async {
    await Firebase.initializeApp();
    _messaging = FirebaseMessaging.instance;
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );
  }

  checkForInitialMessage() async {
    await Firebase.initializeApp();
    RemoteMessage? initialMessage =
    await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      setState(() {
        _totalNotifications++;
      });
    }
  }
  Future<void> backgroundHandler(RemoteMessage message) async {
    print(message.data.toString());
    print(message.notification!.title);
  }
  int currentTap =0;
  void showSMSSomeThingWrong({required String title, required String message}) {
    coShowMessageDialog(
        title: title == null ? 'something_wrong' : title,
        message: message == null ? 'data_response_error' : message,
        messageDialogType: MessageDialogType.fail);
  }
  @override
  void initState(){
    WidgetsFlutterBinding.ensureInitialized();
    FirebaseMessaging.instance.subscribeToTopic("delivery");
    LocalNotificationService.initialize(context);

    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        final routeFromMessage = message.data["route"];
        Navigator.push(context, MaterialPageRoute(builder: (context) =>  NotificationScreen(emplyLeading: false,)));
      }
    });
    FirebaseMessaging.onMessage.listen((message) {
      if (message.notification != null) {
        print(message.notification!.body);
        print(message.notification!.title);
      }
      LocalNotificationService.display(message);
    });

    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      final routeFromMessage = message.data["route"];
      Navigator.push(context, MaterialPageRoute(builder: (context) =>  NotificationScreen(emplyLeading: false,)));
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(currentTap),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedIconTheme: IconThemeData(color:Theme.of(context).primaryColor,),
        selectedItemColor: Theme.of(context).primaryColor,
        unselectedIconTheme: const IconThemeData(color: Colors.grey),
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon:const Icon(Icons.home_outlined),
            label: 'home'.translate().toFirstUppercase(),
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.person_outline),
            label: 'profile'.translate().toFirstUppercase(),
          ),
          BottomNavigationBarItem(
            icon:const Icon(Icons.notifications_none_outlined),
            label: 'notification'.translate().toFirstUppercase(),
          ),
        ],
        currentIndex: currentTap,
        onTap: (val){
          if (val == 1){
          }
          setState(() {
            currentTap = val;
            }
          );
        },
      ),
    );
  }
  void coShowMessageDialog({required String title, required String message, messageDialogType}) {}
}
Widget callPage(int current) {
  switch (current) {
    case 1:
      return  Profile();
    case 2:
      return NotificationScreen(emplyLeading: false,);
    default:
      return HomeScreen();
  }
}
class LocalNotificationService {
  static final FlutterLocalNotificationsPlugin _notificationsPlugin =
  FlutterLocalNotificationsPlugin();
  static void initialize(BuildContext context) {
    const InitializationSettings initializationSettings =
    InitializationSettings(
        android: AndroidInitializationSettings("assets/image/turbotech.png"));
    _notificationsPlugin.initialize(initializationSettings,onSelectNotification: (String? route) async{
      if(route != null){
        Navigator.of(context).pushNamed(route);
      }
    });
  }
  static void display(RemoteMessage message) async {
    try {
      final id = DateTime.now().millisecondsSinceEpoch ~/1000;
      const NotificationDetails notificationDetails =  NotificationDetails(
          android:AndroidNotificationDetails(
            "easyapproach",
            "easyapproach channel",
            importance: Importance.max,
          )
      );
      await _notificationsPlugin.show(id,
        message.notification!.title,
        message.notification!.body,
        notificationDetails,
        payload: message.data["route"],
      );
    } on Exception catch (e) {
      print(e);
    }
  }
}

