import 'kh.dart' as kh;
import 'en.dart' as en;

var lang= 'en';
String tran(String key){
  if(lang == 'en') {
    return _convertLang(en.l[key]) ?? _convertLang(key);
  } else if(lang == 'kh') {
    return _convertLang(kh.l[key]) ?? _convertLang(key);
  } else {
    return _convertLang(en.l[key]) ?? _convertLang(key);
  }
}
String _convertLang(key){
  if(key!= null && key != ''){
    return key.replaceAll('_', ' ');
  }
  return key;
}