import 'package:delivery_app/Screen/Attendance/attendance.dart';
import 'package:delivery_app/Screen/AttendanceList/attendanceList.dart';
import 'package:delivery_app/Screen/AttendanceList/attendanceListDetail.dart';
import 'package:delivery_app/Screen/DeliveryList/daliverylist.dart';
import 'package:delivery_app/Screen/DeliveryList/deliveryListDetail.dart';
import 'package:delivery_app/Screen/Notification/notification.dart';
import 'package:delivery_app/Screen/delivery/customer_delivery.dart';
import 'package:delivery_app/Screen/delivery/delivery.dart';
import 'package:delivery_app/Screen/login/login.dart';
import 'package:delivery_app/Screen/profile/changePassword.dart';
import 'package:delivery_app/Screen/profile/editInaformation.dart';
import 'package:delivery_app/Screen/profile/profile.dart';
import 'package:delivery_app/splash_screen.dart';
import 'package:delivery_app/tap.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Screen/delivery/daliveryDetail.dart';

class RouteGenerator {
  static get arg => null;

 static Route<dynamic>? generateRoute(RouteSettings settings) {
   final args = settings.arguments;
    switch (settings.name) {
      case '/SplashScreen':
        return CupertinoPageRoute(
            builder: (_) => SplashScreen()
        );
      case '/LoginScreen':
        return CupertinoPageRoute(
            builder: (_) => LoginScreen()
        );
      case '/Tap':
        return CupertinoPageRoute(
            builder: (_) =>  Tap()
        );
      case '/attendance':
        return CupertinoPageRoute(
            builder: (_) =>  CheckInOut()
        );
      case '/Profile':
        return CupertinoPageRoute(
            builder: (_) =>  Profile()
        );
      case '/Notification':
        return CupertinoPageRoute(
            builder: (_) =>  NotificationScreen(emplyLeading:  false,)
        );
      case '/changePassword':
        return CupertinoPageRoute(
            builder: (_)=>const ChangePassword()
        );
      case '/updateInformation':
        return CupertinoPageRoute(
            builder: (_)=> EditInformation()
        );
      case '/attendanceLists' :
        return CupertinoPageRoute(
            builder: (_)=> AttendanceList()
        );
      case '/attendanceDetail' :
        return CupertinoPageRoute(
            builder: (_)=>AttendanceListDetail(addresses: '', status:arg , id: '', lat: '', lng: '',)
        );
      case '/deliveryList':
        return CupertinoPageRoute(
            builder: (_)=>const DeliveryList()
        );
      case '/delivery':
        return CupertinoPageRoute(
            builder: (_)=> Delivery(id: '',)
        );
      case '/deliveryDetail':
        return CupertinoPageRoute(
            builder: (_)=> DeliveryDetail(id: '',)
        );
      case '/deliveryCustomer':
        return CupertinoPageRoute(
            builder: (_)=> MapScreen(sellId: '',)
        );
      case '/deliveryListDetail' :
        return CupertinoPageRoute(
            builder: (_)=> DeliveryListDetail(sellId: '',)
        );
      case '/profile ':
        return CupertinoPageRoute(
            builder: (_)=>Profile()
        );
    }
  }
}
