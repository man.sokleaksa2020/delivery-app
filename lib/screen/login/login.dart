import 'package:delivery_app/tap.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flushbar/flushbar.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/utils/g.dart' as g;
import '../../utils/g_widget.dart';
import 'package:delivery_app/helper/stringExtension.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  GlobalKey<FormState> keyForm = GlobalKey<FormState>();
   late int durationInSeconds;
   TextEditingController emailUser = TextEditingController();
   TextEditingController passwordTextController = TextEditingController();

  Future<void> coShowMessageDialog({
    required String title,
    message,
    MessageDialogType messageDialogType = MessageDialogType.successful,
    int durationInSeconds = 3}) async {

    Color color =Theme.of(context).primaryColor;
    IconData icon = Icons.check_circle_outline;
    if(messageDialogType == MessageDialogType.fail){
      color = Colors.redAccent;
      icon = Icons.close;
    } else if(messageDialogType == MessageDialogType.warning){
      color = Colors.orange;
      icon = Icons.info_outline;
    }
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      titleText:Text(title == null ? 'undo_successful': title),
      messageText: Text(message ?? 'item_is_already_undo'),
      icon: Icon(
        icon,
        size: 28.0,
        color: color,
      ),
      duration: Duration(seconds: durationInSeconds),
      animationDuration:const Duration(milliseconds: 500),
      margin:const EdgeInsets.all(8),
      backgroundColor: Colors.white,
      borderColor: color,
      boxShadows:const [
        BoxShadow(
          offset: Offset(0.0, 2.0),
          blurRadius: 10.0,)
      ],
    )
        .show(context);
  }
  void signIn() async{
    String url = "http://172.26.16.113:81/api/auth/login";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map<String, dynamic> fields =  {
      'email' : emailUser.text,
      'password': passwordTextController.text,
    };
    gCo.post(url: url, fields: fields, files: {}).then((value) async {
      final SharedPreferences prefs = await _prefs;
      var res = json.decode(value);
      sharedPreferences.setString('access_token', res['access_token']);
      sharedPreferences.setString('user_role', res['user_role']);
      sharedPreferences.setString('image', res['image']);
      sharedPreferences.setString('username', res['username']);
      sharedPreferences.setString('phone', res['phone']);
      var token = prefs.getString('access_token');
      var role = prefs.getString('user_role');
      var image = prefs.getString('image');
      var name = prefs.getString('username');
      var phone = prefs.getString('phone');
       if(res != null){
        setState(() {
          g.userModel.token = token!;
          g.userModel.role = role!;
          g.userModel.image = image!;
          g.userModel.name = name!;
          g.userModel.phone = phone!;
        });
        if(g.userModel.role == 'DRIVER'){
          setState(() {
            Navigator.push(context, MaterialPageRoute(builder: (context)=> Tap()));
            coShowMessageDialog(
                title: 'well_done'.translate().toFirstUppercaseWord(),
                message: 'your_login_successful'.translate().toFirstUppercaseWord(),
                messageDialogType:MessageDialogType.successful
            );
          });
        }else{
          coShowMessageDialog(
              title: 'login_failed'.translate().toFirstUppercaseWord(),
              message: 'please_check_your_username_and_password'.translate().toFirstUppercase(),
              messageDialogType:MessageDialogType.fail);
        }
      }else{
        coShowMessageDialog(
            title: 'login_failed'.translate().toFirstUppercaseWord(),
            message: 'please_check_your_username_and_password'.translate().toFirstUppercase(),
            messageDialogType:MessageDialogType.fail);
      }
    }).catchError((e) {
      print(e.toString());
      coShowMessageDialog(
          title: 'something_wrong'.translate().toFirstUppercaseWord(),
          message: 'please_check_your_email_and_password'.translate().toFirstUppercaseWord(),
          messageDialogType:MessageDialogType.fail
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Positioned(
                top: -210,
                left: -180,
                child: Container(
                  width: 390,
                  height: 390,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(1000),
                  ),
                  child: Container(
                    margin: const EdgeInsets.fromLTRB(160, 200, 30, 30),
                    width: 60,
                    height: 60,
                    child: Image.asset('assets/image/logo.png'),
                  )
                )),
           SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                color: Colors.transparent,
                child: Stack(
                  children: [
                    Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          margin:const EdgeInsets.only(top: 15),
                          child: Image.asset("assets/image/image_02.png"),
                        )),
                    Form(
                      key: keyForm,
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            padding: const EdgeInsets.only(
                                top: 130, left: 95, right: 95, bottom: 20),
                            child:
                            Image.asset('assets/image/500_F_332331038_q8yMIz7gccfsjw8yvGXoB53uEd1xX48D.png'),
                          ),
                          Container(
                            margin: const  EdgeInsets.symmetric(horizontal: 5),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15)
                            ),
                            child: Column(
                              children: [
                                Container(
                                  padding:
                                  const EdgeInsets.only(top: 15, bottom: 15),
                                  child: TextFormField(
                                    controller: emailUser,
                                    keyboardType: TextInputType.emailAddress,
                                    autofocus: false,
                                    decoration: InputDecoration(
                                      hintText: 'email'.translate().toFirstUppercase(),
                                      hintStyle: const TextStyle(
                                        color: Colors.grey
                                      ),
                                      contentPadding: const EdgeInsets.fromLTRB(20.0, 17.0, 20.0, 17.0),
                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.only( bottom: 15),
                                  child: TextFormField(
                                    controller: passwordTextController,
                                    autovalidateMode: AutovalidateMode.always,
                                   obscureText: true,
                                    decoration: InputDecoration(
                                      hintText: 'password'.translate().toFirstUppercase(),
                                      hintStyle: const TextStyle(
                                          color: Colors.grey
                                      ),
                                      contentPadding: const EdgeInsets.fromLTRB(20.0, 17.0, 20.0, 17.0),
                                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0),),
                                    ),
                                  ),
                                ),
                                Center(
                                  child: TextButton(
                                  onPressed: () {
                                    signIn();
                                  },
                                  child:Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 0, vertical: 15),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(30),
                                          color: Theme.of(context).primaryColor
                                      ),
                                      child:  Center(
                                        child: Text('login'.translate().toFirstUppercase(), style: const TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold
                                         ),
                                        ),
                                      )
                                    )
                                 ),
                               )
                             ]
                           ),
                          )
                        ],
                      ),
                    ),
                    Positioned(
                        top:350,
                        right: -25,
                        child: Container(
                          width: 48,
                          height: 48,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(1000)),
                        )),
                    Positioned(
                        bottom: 70,
                        left: -25,
                        child: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              color:Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(1000)),
                        )
                    ),
                    Positioned(
                        bottom: 110,
                        right:79,
                        child: Container(
                          width: 16,
                          height: 16,
                          decoration: BoxDecoration(
                              color:Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(1000),),
                        )
                     )
                  ],
                ),
              ),
            ),
          ],
        )
    );
  }
}
