// ignore_for_file: unnecessary_null_comparison

import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:delivery_app/Screen/login/login.dart';
import 'package:delivery_app/Screen/profile/privacyAndpolicy.dart';
import 'package:delivery_app/helper/stringExtension.dart';

import 'package:delivery_app/model/user_model.dart';
import 'package:delivery_app/screen/profile/aboutApp.dart';
import 'package:delivery_app/screen/profile/changePassword.dart';
import 'package:delivery_app/screen/profile/editInaformation.dart';
import 'package:delivery_app/screen/profile/terms.dart';
import 'package:delivery_app/utils/appmodel.dart';
import 'package:delivery_app/utils/g_co.dart';
import 'package:delivery_app/utils/g_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:http/http.dart' as http;
import 'package:scoped_model/scoped_model.dart';
import 'package:delivery_app/utils/g_co.dart'as gCo;

class Profile extends StatefulWidget {

  @override
  _ProfileState createState() => _ProfileState();
}
class _ProfileState extends State<Profile> {
  late LanguageModel languageModelVal;
  late String lang = 'en';
  late ScrollController _scrollController;
  late bool emplyLeading;
  bool lastStatus = false;

  get arge => null;
  List<LanguageModel> getLanguage() {
    List<LanguageModel> iconList = [];
    iconList.add(
        LanguageModel(
            key: 'en',
            name: ' english'.translate(),
            assetImage: 'assets/image/en-en.png',
            other: (){}));
    iconList.add(
        LanguageModel(
            key: 'kh',
            name: ' khmer'.translate().toFirstUppercase(),
            assetImage: 'assets/image/kh-kh.png',
            other: (){}));
    return iconList;
  }
  _scrollListener() {
    if (isShrink != lastStatus) {
      setState(() {
        lastStatus = isShrink;
        print(lastStatus);
      });
    }
  }
  bool get isShrink {
    return _scrollController.hasClients &&
        _scrollController.offset > (200 - kToolbarHeight);
  }
  newDialog(BuildContext context, {String title : 'title', required Widget build, required Color headColor, height : 450.0}){
    headColor = (headColor ?? Theme.of(context).primaryColor);
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: Colors.transparent,
          child: Container(
            height: height,
            color: Colors.transparent,
            child: Column(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: headColor,
                    borderRadius: const BorderRadius.only(
                        topLeft:  Radius.circular(10.0),
                        topRight: Radius.circular(10.0)),
                  ),
                  padding:const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Center(
                    child: Text(
                      title,
                      style: const TextStyle(color: Colors.white,fontWeight: FontWeight.bold, fontSize: 17),
                    ),
                  ),
                ),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10.0),
                          bottomRight:  Radius.circular(10.0)),
                    ),
                    child: build ?? coZero()
                ),
              ],
            ),
          ),
        ) ;
      },
    );
  }
  Future<http.Response> logout() {
    return http.post(
      Uri.parse('http://172.26.16.113:81/api/logout'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
  }
  coShowConfirm({context, title,required String content, yesIsDestructiveAction: true, required Widget buildContent}){
    return  showDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text('${title ?? 'are_you_sure'.translate().toFirstUppercaseWord()}',textAlign: TextAlign.center, style: const TextStyle(color: Colors.black)),
        content: buildContent ?? Text('${content == null ? 'you_want_to_delete_this_item' : content}',textAlign: TextAlign.center, style: TextStyle(color: Colors.black)),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () async {
              Navigator.of(context).pop(true);
            },
            child:  Text('no'.translate().toFirstUppercase()),
          ),
          CupertinoDialogAction(
            isDestructiveAction: yesIsDestructiveAction,
            onPressed: () {
              g.logout(context);
             Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
            },
            child:  Text('yes'.translate().toFirstUppercase()),
          ),
        ],
      ),
    );
  }
  void changeLanguage(context, { required String title , required ValueChanged onChanged}){
    newDialog(context,
        title: title ?? 'choose_languages'.translate().toFirstUppercaseWord(),
        build: (getLanguage() == null ? false :getLanguage().isNotEmpty) ? Column(
          mainAxisSize: MainAxisSize.min,
          children: List.generate(getLanguage().length,(index){
            return RadioListTile(
              activeColor: Theme.of(context).primaryColor,
              title: Row(
                children: <Widget>[
                  SizedBox(
                      width: 40,
                      child: Image.asset(getLanguage()[index].assetImage)
                  ),
                  const SizedBox(width: 5,),
                  Text(getLanguage()[index].name,),
                ],
              ),
              value: getLanguage()[index].key,
              groupValue: lang,
              onChanged: (value) {
                languageModelVal = getLanguage()[index];
                lang = getLanguage()[index].key;
                Navigator.of(context).pop();
                if(onChanged != null){
                  onChanged(value);
                }
              },
            );
          }),
        ) :
         Text('languages'.translate().toFirstUppercase(),), headColor: Theme.of(context).primaryColor
    ) ;
  }

  List<UserModel> parseProducts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<UserModel>((json) => UserModel.fromJson(json)).toList();
  }
  Future<List<UserModel>> fetchAlbum() async {
    print('profile token'+g.userModel.token);
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/users'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      return parseProducts(response.body);
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
  }
  @override
  Widget build(BuildContext context) {
    return  Material(
      color: Colors.grey[300],
      child: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: Container(
              color: Colors.white,
              height: 600,
            ),
          ),
        FutureBuilder<List<UserModel>>(
        future: fetchAlbum(),
        builder: (context, snapshot) {
        if (snapshot.hasData) {
       print(snapshot.data!.length);
      List<UserModel> userInfo = snapshot.data!;
         return Stack(
           children:List.generate(userInfo.length, (index) {
           return CustomScrollView(
           physics: const BouncingScrollPhysics(),
           controller: _scrollController,
           slivers: <Widget>[
            SliverAppBar(
              brightness: Brightness.dark,
              backgroundColor: lastStatus ? Theme.of(context).primaryColor : Colors.white,
              title: Text('profile'.translate().toFirstUppercase(),),
              pinned: true,
              expandedHeight: 335.0,
              flexibleSpace: FlexibleSpaceBar(
                background: Column(
                  children: <Widget>[
                    Container(
                      decoration:  BoxDecoration(
                              color:Theme.of(context).primaryColor,
                              borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(30.0),
                              bottomRight: Radius.circular(30.0))
                      ),
                      padding: const EdgeInsets.symmetric(vertical: 30.0),
                      width: double.infinity,
                      child: Center(
                          child: Column(
                            children: <Widget>[
                              const SizedBox(
                                height: 55.0,
                              ),
                              Container(
                                  decoration: const BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(75.0),
                                      )),
                                  padding: const EdgeInsets.all(7),
                                  width: 150.0,
                                  height: 150.0,
                                  child:
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child:(userInfo[index].image != '')? Image.network('http://172.26.16.113:81/'+userInfo[index].image, fit: BoxFit.cover,):
                                    Image.asset('assets/image/Profile-PNG-Images.png'),
                                  )
                              ),
                               const SizedBox(height: 15.0,),
                               Text(userInfo[index].name, style:const TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold
                              ),),
                              Container(
                                padding: const EdgeInsets.only(top: 5.0),
                                child:  Text(g.convertPhoneNumber(userInfo[index].phone), style:const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16
                                ),),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                            ],
                          )
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                <Widget>[
                  Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const SizedBox(height: 10.0,),
                          Container(
                            padding:const  EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
                            width: double.infinity,
                            color: Colors.white,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                                  child:  Text('account'.translate().toFirstUppercase(), textAlign: TextAlign.left, style: const TextStyle(fontSize: 26.0, color: Colors.black87, fontWeight: FontWeight.bold),),
                                ),
                                const Divider(
                                  color: Colors.grey,
                                  height: 20,
                                ),
                                ListTile(
                                    leading: Icon(Icons.vpn_key_outlined, color: Theme.of(context).primaryColor,),
                                    title: Text('change_password'.translate().toFirstUppercaseWord()),
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> const ChangePassword()));
                                    }
                                ),
                                ListTile(
                                    leading: Icon(Icons.perm_identity, color: Theme.of(context).primaryColor,),
                                    title:  Text('update_information'.translate().toFirstUppercaseWord()),
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const EditInformation()));
                                    }
                                ),
                                ListTile(
                                  leading: Icon(Icons.language, color: Theme.of(context).primaryColor,),
                                  title:  Text('change_languages'.translate().toFirstUppercaseWord()),
                                    onTap: () {
                                       changeLanguage(context,
                                          onChanged: (v){
                                              AppModel appModel = ScopedModel.of(context);
                                              appModel.userActionss(language: v).then((value){
                                              Navigator.pushNamedAndRemoveUntil(context, '/LoginScreen', (_) => false);
                                            });
                                          }, title: 'language'.translate().toFirstUppercase()
                                      );
                                    }
                                ),
                                const SizedBox(height: 10.0,),
                                Container(
                                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                                  child:  Text('about'.translate().toFirstUppercase(), textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, color: Colors.grey),),
                                ),
                                const Divider(
                                  color: Colors.grey,
                                  height: 20,
                                ),
                                ListTile(
                                    leading: Icon(Icons.help, color: Theme.of(context).primaryColor,),
                                    title:  Text('about_app'.translate().toFirstUppercaseWord()),
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const AboutApp()));
                                    }
                                ),
                                ListTile(
                                    leading: Icon(Icons.assignment, color: Theme.of(context).primaryColor,),
                                    title:  Text('privacy_policy'.translate().toFirstUppercaseWord()),
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>const Privacy()));
                                    }
                                ),
                                ListTile(
                                    leading: Icon(Icons.content_paste, color: Theme.of(context).primaryColor,),
                                    title:  Text('terms_and_conditions'.translate().toFirstUppercaseWord()),
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> const TermsAndCondition()));
                                    }
                                ),
                                const Divider(
                                  color: Colors.grey,
                                  height: 20,
                                ),
                                Center(
                                    child: InkWell(
                                      child: Container(
                                        padding: const EdgeInsets.symmetric(vertical: 6.0),
                                        child:  Text('logout'.translate().toFirstUppercase(), style:const TextStyle(fontSize: 20.0, color: Colors.red, fontWeight: FontWeight.bold),),
                                      ),
                                      onTap: () {
                                        coShowConfirm(
                                            context: context,
                                            content: "",
                                            buildContent:  Text("do_you_want_to_logout_?".translate().toFirstUppercase()));
                                        },
                                    )
                                 )
                              ],
                            ),
                          ),
                          Container(
                            height: 200.0,
                            color: Colors.grey[300],
                          )
                        ],
                      )
                  ),
                ],
              ),
            ),
          ],);
        }),
      );
    }
       else if (snapshot.hasError) {
           return Text('${snapshot.error}');
          }
          return const Center(
           child: CircularProgressIndicator(
           color: Colors.amber,
            ),
          );
         }
       )
        ],
      ),
    );
  }
  coZero() {}
}
  class LanguageModel{
  String key;
  String name;
  String assetImage;
  dynamic other;
  LanguageModel({required this.key, required this.name, required this.assetImage, this.other});
}