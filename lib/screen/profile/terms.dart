import 'dart:convert';
import 'package:delivery_app/model/privacy.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:http/http.dart' as http;
import 'package:delivery_app/helper/stringExtension.dart';

class TermsAndCondition extends StatefulWidget {
  const TermsAndCondition({Key? key}) : super(key: key);

  @override
  _TermsAndConditionState createState() => _TermsAndConditionState();
}

class _TermsAndConditionState extends State<TermsAndCondition> {
  List<Policy> parsePhotos(String responseBody) {
    final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Policy>((json) => Policy.fromJson(json)).toList();
  }
  Future<List<Policy>> fetchPhotos(http.Client client) async {
    final response = await client
        .get(Uri.parse('http://172.26.16.113:81/api/termsAndConditions'));
    return parsePhotos(response.body);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        elevation: 0.0,
        brightness: Brightness.dark,
        title:  Text("terms_and_conditions".translate().toFirstUppercaseWord()),
      ),
      body:FutureBuilder<List<Policy>>(
        future: fetchPhotos(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(
              child: Text('An error has occurred!'),
            );
          } else if (snapshot.hasData) {
            List<Policy> userInfo = snapshot.data!;
            return ListView(
              children: List.generate(userInfo.length, (index) {
                return Column(
                  children: [
                    Text(g.convertModelString(userInfo[index].description.toString()))
                  ],
                );
              }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
