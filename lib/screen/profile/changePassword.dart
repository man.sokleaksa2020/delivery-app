import 'dart:convert';
import 'dart:io';
import 'package:delivery_app/utils/g_widget.dart' as gWidget;
import 'package:delivery_app/utils/g.dart' as g;
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../utils/g_widget.dart';
import 'package:delivery_app/helper/stringExtension.dart';

class ChangePassword extends StatefulWidget {
  const ChangePassword({Key? key}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController password = TextEditingController();
  TextEditingController oldPassword = TextEditingController();
  Future<void> coShowMessageDialog({
    required String title,
    message,
    MessageDialogType messageDialogType = MessageDialogType.successful,
    int durationInSeconds = 3}) async {

    Color color = Colors.green;
    IconData icon = Icons.check_circle_outline;
    if(messageDialogType == MessageDialogType.fail){
      color = Colors.redAccent;
      icon = Icons.close;
    } else if(messageDialogType == MessageDialogType.warning){
      color = Colors.orange;
      icon = Icons.info_outline;
    }
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      titleText:Text(title == null ? 'undo_successful': title),
      messageText: Text(message == null ? 'item_is_already_undo' : message),
      icon: Icon(
        icon,
        size: 28.0,
        color: color,
      ),
      duration: Duration(seconds: durationInSeconds),
      animationDuration:const Duration(milliseconds: 500),
      margin:const EdgeInsets.all(8),
      //borderRadius: ,
      backgroundColor: Colors.white,
      borderColor: color,
      boxShadows:const [
        BoxShadow(
          offset: Offset(0.0, 2.0),
          blurRadius: 3.0,)
      ],
    )
        .show(context);
  }
  void createAlbum(String oldPass, String newPass) async {
    gWidget.onLoading(context);
    final response = await http.put(
      Uri.parse('http://172.26.16.113:81/api/users'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, String>{
        'old_password': password.text,
        'new_password': oldPassword.text,
      }
      ),
    );
    if (response.statusCode == 200) {
      print(response);
      Navigator.of(context, rootNavigator: true).pop(true);
      coShowMessageDialog(
          title: 'Well Done',
          message: 'Your login successful',
          messageDialogType:MessageDialogType.successful
      );
    }
    else{
      Navigator.of(context, rootNavigator: true).pop(true);
      coShowMessageDialog(
          title: 'Fail',
          message: 'please check it again',
          messageDialogType:MessageDialogType.fail
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Theme.of(context).primaryColor,
        brightness: Brightness.dark,
        title: Text("change_password".translate().toFirstUppercaseWord()),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            padding:const EdgeInsets.symmetric(horizontal:15, vertical: 10),
            child: TextFormField(
              controller: oldPassword,
              decoration:  InputDecoration(
                border:const OutlineInputBorder(
                ),
                hintText: 'old_password'.translate().toFirstUppercaseWord(),
                hintStyle: const TextStyle(
                  color: Color(0xffbdbdbd),
                ),
              ),
            ),
          ),
          Container(
            padding:const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: TextFormField(
              controller: password,
              decoration:  InputDecoration(
                border: const OutlineInputBorder(
                ),
                hintText: 'new_password'.translate().toFirstUppercaseWord(),
                hintStyle: const TextStyle(
                  color: Color(0xffbdbdbd),
                ),
              ),
              validator: ( value){
                if(value!.isEmpty)
                {
                  return 'Please re-enter password';
                }
                if(password.text!= password.text){
                  return "Password does not match";
                }
                return null;
              },
            ),
          ),
          Container(
            padding:const EdgeInsets.symmetric(horizontal:15, vertical: 10),
            child: TextButton(
              onPressed: (){
                createAlbum(oldPassword.text,password.text);
              },
              child:Container(
                padding:const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Theme.of(context).primaryColor),
                    child: Text("save".translate().toFirstUppercase(), style:const TextStyle(
                    color: Colors.white,
                    fontSize: 23,
                    fontWeight: FontWeight.bold,
                   ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
