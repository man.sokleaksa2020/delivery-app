import 'dart:convert';
import 'dart:io';
import 'package:delivery_app/Screen/profile/profile.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../model/user_model.dart';
import 'package:camera/camera.dart';
import 'package:http/http.dart' as http;
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/helper/stringExtension.dart';

class EditInformation extends StatefulWidget {


  const EditInformation({Key? key, }) : super(key: key);

  @override
  _EditInformationState createState() => _EditInformationState();
}

class _EditInformationState extends State<EditInformation> {
  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phone = TextEditingController();
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  PickedFile? imageFile ;
  late CameraController _controllers;

  late File _image;
  final picker = ImagePicker();

  imgFromCamera() async {
    File image = (await picker.pickImage(
        source: ImageSource.camera, imageQuality: 50)) as File;
    setState(() {
      _image = image;
    });
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: const Icon(Icons.photo_library),
                    title: const Text('Photo Library'),
                    onTap: () {
                      getImage();
                      Navigator.of(context).pop();
                    }),
                ListTile(
                  leading: const Icon(Icons.photo_camera),
                  title: const Text('Camera'),
                  onTap: () {
                    imgFromCamera();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }
  List<UserModel> parseProducts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<UserModel>((json) => UserModel.fromJson(json)).toList();
  }

  Future<List<UserModel>> fetchAlbum() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/users'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      return parseProducts(response.body);
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<http.Response> updateInformation(String name, String email, String phone, ) {
    return http.post(Uri.parse('http://172.26.16.113:81/api/users'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
        },
        body: jsonEncode(<String, dynamic>{
          'name': name,
          'email': email,
          'phone': phone,
        }));
  }

  Future<http.Response> updateData(String pass) {
    return http.put(
      Uri.parse('http://172.26.16.113:81/api/users'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, String>{
        'new_password': pass,
      }),
    );
  }


  Future<http.Response> updateimage(PickedFile image) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return http.post(Uri.parse('http://172.26.16.113:81/api/upload'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, dynamic>{
        'file[]': image,}),
    );
  }
  void upload() async{
    String url = "http://172.26.16.113:81/api/upload";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map<String, File> files =  {
      'file[]' : _image,
    };
    gCo.post(url: url, fields: {}, files: files).then((value) async {
      final SharedPreferences prefs = await _prefs;
      var res = json.decode(value);
     sharedPreferences.setString('access_token', res['access_token']);
      var token = prefs.getString('access_token');
      if(res != null){
        setState(() {
         g.userModel.token = token!;
          print(res);
        });
      }else{

      }
    }).catchError((e) {
      print(e.toString());

    });
  }

  Future<XFile> takePicture()  async {
    if (_controllers.value.isTakingPicture) {
      return takePicture();
    }
    try {
      XFile file = await _controllers.takePicture();
      return file;
    } on CameraException catch (e) {
      print(e);
      return takePicture();
    }
  }

  void _openCamera(BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
    );
    setState(() {
      imageFile = pickedFile!;
    });
    Navigator.pop(context);
  }

  void _openGallery(BuildContext context) async {
    final pickedFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
    );
    setState(() {
      imageFile = pickedFile;
      upload();



    });

    Navigator.pop(context);
  }

  Future<void> _showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text(
              "Choose option",
              style: TextStyle(color: Color(0xffAA8043)),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  const Divider(height: 1, color: Color(0xffAA8043)),
                  ListTile(
                    onTap: () {
                      _openGallery(context);
                    },
                    title: const Text("Gallery"),
                    leading:
                    const Icon(Icons.account_box, color: Color(0xffAA8043)),
                  ),
                  const Divider(height: 1, color: Color(0xffAA8043)),
                  ListTile(
                    onTap: () {
                      _openCamera(context);
                    },
                    title: const Text("Camera"),
                    leading: const Icon(Icons.camera, color: Color(0xffAA8043)),
                  ),
                ],
              ),
            ),
          );
     });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          brightness: Brightness.dark,
          elevation: 0.0,
          title: Text("update_information".translate().toFirstUppercaseWord()),
        ),
        body: FutureBuilder<List<UserModel>>(
            future: fetchAlbum(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                print(snapshot.data!.length);
                List<UserModel> userInfo = snapshot.data!;
                return Stack(
                    children: List.generate(
                        userInfo != null ? userInfo.length : 0, (index) {
                      return ListView(
                        children: [
                          Form(
                            key: formKey,
                            child: Column(
                              children: <Widget>[
                                Container(
                                    padding:
                                    const EdgeInsets.fromLTRB(15, 50, 15, 5),
                                    child: Stack(
                                      children: <Widget>[
                                        Container(
                                          decoration: BoxDecoration(
                                              color: Theme.of(context).primaryColor,
                                              borderRadius: const BorderRadius.all(
                                                Radius.circular(75.0),
                                              )),
                                          height: MediaQuery.of(context).size.width * 0.38,
                                          width: MediaQuery.of(context).size.width * 0.38,
                                          child: ClipRRect(
                                              borderRadius: BorderRadius.circular(200),
                                              child:
                                              (imageFile != null ? Image.file( File(  imageFile!.path ,), fit: BoxFit.cover,):Image.network(
                                                'http://172.26.16.113:81/' +
                                                    userInfo[index].image, fit: BoxFit.cover,))
                                          ),),
                                        Positioned(
                                          right: 10,
                                          bottom: 10,
                                          child: CircleAvatar(
                                            radius: 20,
                                            backgroundColor: Colors.grey[400],
                                            child: IconButton(
                                              icon: const Icon(
                                                Icons.camera_alt,
                                                color: Colors.black,
                                                size: 25.0,
                                              ),
                                              onPressed: () {
                                                _showChoiceDialog(context);
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                                const SizedBox(
                                  height: 10,
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 10),
                                  child: TextFormField(
                                    controller: name,
                                    decoration:  InputDecoration(
                                      border:const OutlineInputBorder(),
                                      hintText: 'name'.translate().toFirstUppercase(),
                                      hintStyle:const TextStyle(
                                        color: Color(0xffbdbdbd),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 10),
                                  child: TextFormField(
                                    controller: email,
                                    decoration:  InputDecoration(
                                      border:const OutlineInputBorder(
                                          borderSide: BorderSide(
                                            color: Color(0xFF000000),
                                          )),
                                      hintText: 'email'.translate().toFirstUppercase(),
                                      hintStyle:const TextStyle(
                                        color: Color(0xffbdbdbd),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 10),
                                  child: TextFormField(
                                    controller: phone,
                                    keyboardType: TextInputType.phone,
                                    decoration:  InputDecoration(
                                      border:const OutlineInputBorder(),
                                      hintText: 'phone_number'.translate().toFirstUppercaseWord(),
                                      hintStyle:const TextStyle(
                                        color: Color(0xffbdbdbd),
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                TextButton(
                                    onPressed: () {
                                        setState(() {
                                          updateInformation(
                                            name.text,
                                            email.text,
                                            phone.text,
                                          );
                                        });
                                     Navigator.push(context, MaterialPageRoute(builder: (context)=>Profile()));} ,
                                    child: Container(
                                       width: MediaQuery.of(context).size.width * 1,
                                        padding: const EdgeInsets.symmetric(
                                           vertical: 15),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            color: Theme.of(context).primaryColor),
                                        child:  Center(
                                          child: Text(
                                            'save'.translate().toFirstUppercase(),
                                            style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 21,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ))),
                                const SizedBox(
                                  height: 50,
                                )
                              ],
                            ),
                          ),
                        ],
                      );
                    }));
                 }
              return const Center(
                child: CircularProgressIndicator(color: Colors.amber),
              );
            }
          )
        );
    }
}
