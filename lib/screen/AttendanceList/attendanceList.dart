import 'dart:io';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/Screen/AttendanceList/attendanceListDetail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../../model/attendance_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:delivery_app/utils/g_form.dart' as gForm;
import 'package:delivery_app/helper/stringExtension.dart';

class AttendanceList extends StatefulWidget {
  late String lat;
  late String lng;
 late String title;
  late  Color color;
  late IconData icon;
 late  GestureTapCallback onTap;
 late  String value = '';
 late String filter ;
  DateTime _currentdate = DateTime.now();

  AttendanceList({Key? key,}) : super(key: key);

  @override
  _AttendanceListState createState() => _AttendanceListState();
}

class _AttendanceListState extends State<AttendanceList> {
  String formattedDate = DateFormat('EEE d MMM yyyy').format(now);
   DateFormat formatter = DateFormat('MM/dd/yyyy');
  TextEditingController date = TextEditingController();
  late DateTime filterDate = DateTime.now() ;
  DateTime dates = DateTime(2016, 10, 26);
  static DateTime get now => DateTime.now();
  late DateTime selectedDate = DateTime.now();
  String formattedTime = DateFormat('kk:mm:ss a').format(now);
  TextEditingController dateinput = TextEditingController();
  String dateTimeToFormatTimeWithSecond(DateTime now) {
    var outputFormat = DateFormat('hh:mm a');
    return outputFormat.format(now);
  }
  late bool _isLoading;
  int currentPage = 1;
  late int totalPages;
  List<Data> passengers = [];
  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  Future<bool> getPassengerDatas({bool isRefresh = false,}) async {

    if (isRefresh) {
      currentPage = 1;
    } else {
      if (currentPage >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    final Uri uri = Uri.parse(
        "http://172.26.16.113:81/api/attendance?filterAttendanceDate=${g.stringDateTimeToFormatDates(filterDate.toString())}");
    final response = await http.get(
      uri,
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      final result = passengersDataFromJson(response.body);
      if (isRefresh) {
        passengers = result.data;
      } else {
        passengers.addAll(result.data);
      }
      currentPage++;
      totalPages = result.lastPage;
      setState(() {});
      return true;
    } else {
      return false;
    }
  }
  void showCupertinoPicker<T>(BuildContext context,
      {bool isDatePicker = false,
        required List<T> options,
        required T selectedValue,
        required DateTime minDate,
        required DateTime lastDate,
        required DateTime initialDate,
        required Function(T) onChanged}) {
    showCupertinoModalPopup(
      context: context,
      builder: (context) => SizedBox(
        height: 240.0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              alignment: Alignment.centerRight,
              decoration: const BoxDecoration(
                color:  Color.fromRGBO(249, 249, 247, 1.0),
                border: Border(
                  bottom:  BorderSide(width: 0.5, color: Colors.black38),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  CupertinoButton(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Text(
                      'Cancel',
                      style: CupertinoTheme.of(context).textTheme.actionTextStyle.copyWith(
                        fontWeight: FontWeight.w600,
                        color: Colors.red,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  CupertinoButton(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    child: Text(
                      'Done',
                      style: CupertinoTheme.of(context).textTheme.actionTextStyle.copyWith(fontWeight: FontWeight.w600),
                    ),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                ],
              ),
            ),
            Expanded(
              child: isDatePicker ? CupertinoDatePicker(
                minimumDate: minDate,
                maximumDate: lastDate,
                initialDateTime: initialDate,
                mode: CupertinoDatePickerMode.date,
                onDateTimeChanged: (DateTime value) {
                onChanged(value as T);
                },
              )
                  : CupertinoPicker.builder(
                backgroundColor: Colors.white,
                childCount: options.length,
                itemBuilder: (BuildContext context, int index) => const Center(
                  child: Text(''),
                ),
                itemExtent: 35.0,
                scrollController: selectedValue != null
                    ? FixedExtentScrollController(initialItem: options.indexOf(options.singleWhere((val) => val == selectedValue)))
                    : null,
                onSelectedItemChanged: (int index) => onChanged(options[index]),
              ),
            ),
          ],
        ),
      ),
    );
  }
  void _showDatePicker(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) => Container(
              height: 500,
              color: const Color.fromARGB(255, 255, 255, 255),
              child: Column(
                children: [
                  SizedBox(
                    height: 400,
                    child: CupertinoDatePicker(
                        initialDateTime: DateTime.now(),
                        onDateTimeChanged: (val) {
                          setState(() {
                            selectedDate = val;
                          });
                        }),
                  ),

                  // Close the modal
                  CupertinoButton(
                    child: const Text('OK'),
                    onPressed: () => Navigator.of(ctx).pop(),
                  )
                ],
              ),
            ));
  }

  void refreshState(){
    setState(() {
    });
  }
  void clearFilter(){
    filterDate == null;
    date == null;
  }
  void _showDialog(Widget child) {
    showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) => Container(
          height: 216,
          padding: const EdgeInsets.only(top: 6.0),
          margin: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          color: CupertinoColors.systemBackground.resolveFrom(context),
          child: SafeArea(
            top: false,
            child: child,
          ),
        ));
  }

  @override
  void initState() {
    _isLoading = true;
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        _isLoading = false;
      });
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Stack(children: <Widget>[
      SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: _header(context),
      ),
      Positioned(
          bottom: -10,
          left: -130,
          child: _circularContainer(width * .7, Colors.transparent,
              borderColor: Colors.red.withOpacity(0.3))),
      Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            automaticallyImplyLeading: true,
            brightness: Brightness.dark,
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Column(
              children:  <Widget>[
                Text(
                  'attendance_lists'.translate().toFirstUppercaseWord(),
                ),
              ],
            ),
          ),
          body: Column(
            children: [
              Container(
                  height: 30,
                  margin: const EdgeInsets.symmetric(vertical: 20),
                  child: ListView(
                      scrollDirection: Axis.horizontal,
                      physics: const BouncingScrollPhysics(),
                      children: [
                        const SizedBox(width: 10,),
                        gForm.CoDate(
                          text: 'filter_date'.translate().toFirstUppercaseWord(),
                          initialDateTime: DateTime.now(),
                          dateFormatDisplay:  'd MMM, y',
                          onChanged: (v){
                            filterDate = v;
                            print('===============================');
                            print(g.stringDateTimeToFormatDates(filterDate.toString()));
                            refreshState();
                            setState(() {
                            });
                          },
                          customBuilder: (context, val){
                            return Container(
                              padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.blueAccent
                              ),
                              child:Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                   Text('filter_date'.translate().toFirstUppercaseWord(), style:const TextStyle(fontSize: 14 ,color: Colors.white, fontWeight: FontWeight.bold),),
                                  const SizedBox(width: 5,),
                                  filterDate != null ? Text(': ${g.stringDateTimeToFormatDays(filterDate.toString())}', style:const TextStyle(fontSize: 14 ,color: Colors.white),) : const SizedBox(width: 0, height: 0,),
                                  const Icon(Icons.date_range, size: 16, color: Colors.white)
                                ],
                              )
                            );
                          },
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ])),
              Expanded(
                child: SmartRefresher(
                  controller: refreshController,
                 enablePullUp: false,
                  onRefresh: () async {
                    final result = await getPassengerDatas(isRefresh: true);
                    if (result) {
                      refreshController.refreshCompleted();
                    } else {
                      refreshController.loadFailed();
                    }
                  },
                  onLoading: () async {
                    final result = await getPassengerDatas();
                    if (result) {
                      refreshController.loadComplete();
                    } else {
                      refreshController.loadFailed();
                    }
                  },
                  child: ListView.separated(
                    itemBuilder: (context, index) {
                      final passenger = passengers[index];
                      print('object===============================');
                      print(passenger);
                      return InkWell(
                        child: Container(
                            margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                    color: Theme.of(context).hintColor.withOpacity(0.15),
                                    offset: const Offset(2, 4),
                                    blurRadius: 5),
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                        margin: const EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 10),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        child: (passenger.attendanceStatus == 0)
                                            ? Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 25,
                                                        vertical: 5),
                                                decoration: BoxDecoration(
                                                    color: Colors.green,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25)),
                                                child:  Text(
                                                  'in'.translate().toFirstUppercase(),
                                                  style: const TextStyle(
                                                      color: Colors.white),
                                                ),
                                              )
                                            : Container(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 20,
                                                        vertical: 5),
                                                decoration: BoxDecoration(
                                                    color: Colors.red,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25)),
                                                child:  Text(
                                                  'out'.translate().toFirstUppercase(),
                                                  style:const TextStyle(
                                                      color: Colors.white),
                                                ),
                                              )),
                                          ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 15),
                                          child: Text(
                                            passenger.driverName,
                                            style: const TextStyle(
                                                fontSize: 16,
                                                color: Colors.blue,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                             child: Text(g.stringDateTimeToFormatDays(passenger.updatedAt),
                                            style: const TextStyle(
                                                fontSize: 16,
                                                color: Colors.black),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          child: Text(
                                            g.stringDateTimeToFormatTimes(
                                                passenger.createdAt),
                                            style: const TextStyle(
                                              fontSize: 15,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.location_on_outlined,
                                        color: Colors.grey[500],
                                      ),
                                      Container(
                                        width: MediaQuery.of(context).size.width/2,
                                        padding: const EdgeInsets.symmetric(horizontal: 10),
                                        child: Text(
                                          passenger.attendanceAddress,
                                          style: TextStyle(
                                              fontSize: 15,
                                              color: Colors.grey[600]),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 15,
                                )
                              ],
                            )),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => AttendanceListDetail(
                                        lat: passenger.latitude,
                                        id: passenger.id,
                                        lng: passenger.longitude,
                                        addresses: passenger.attendanceAddress,
                                        status: passenger.attendanceStatus,
                               )));
                           },
                      );
                    },
                    separatorBuilder: (context, index) => Container(),
                    itemCount: passengers.length,
                  ),
                ),
              )
            ],
          )
       )
    ]);
  }
}

Widget _header(BuildContext context) {
  var width = MediaQuery.of(context).size.width;
  return Container(
    color: Colors.grey[200],
    child: Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
          child: Container(
              height: 250,
              width: width,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              child: Stack(
                fit: StackFit.expand,
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                      top: 40,
                      left: 0,
                      child: Container(
                          width: width,
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const <Widget>[
                              SizedBox(height: 10),
                              SizedBox(height: 20),
                            ],
                          )))
                ],
              )),
        ),
      ],
    ),
  );
}

Widget _circularContainer(double height, Color color,
    {Color borderColor = Colors.transparent, double borderWidth = 2}) {
  return Container(
    height: height,
    width: height,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: color,
      border: Border.all(color: borderColor, width: borderWidth),
    ),
  );
}
class _DatePickerItem extends StatelessWidget {
  const _DatePickerItem({required this.children});
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(
            color: CupertinoColors.inactiveGray,
            width: 0.0,
          ),
          bottom: BorderSide(
            color: CupertinoColors.inactiveGray,
            width: 0.0,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: children,
        ),
      ),
    );
  }
}