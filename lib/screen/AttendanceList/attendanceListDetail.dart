import 'dart:convert';
import 'dart:io';
import 'package:delivery_app/model/attendance_detail.dart';
import 'package:flutter/scheduler.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/helper/stringExtension.dart';

class AttendanceListDetail extends StatefulWidget {
  late String id;
  late String lat = '';
  late String lng = '' ;
  late String addresses = '';
  late int status  ;
   AttendanceListDetail({Key?  key,required this.addresses,required this.status, required this.id, required this.lat, required this.lng}) : super(key: key);

  @override
  _AttendanceListDetailState createState() => _AttendanceListDetailState();
}

class _AttendanceListDetailState extends State<AttendanceListDetail> {
  late GoogleMapController mapController;
  Set<Marker> _markers = {};
  static const LatLng _center = LatLng(11.582672483525592, 104.90029403637303);
  bool alreadyLoading = false;
  bool error = false;
  late BitmapDescriptor pinLocationIcon;
  LatLng pinPosition =  const LatLng(11.582672483525592, 104.90029403637303);
  late  BitmapDescriptor pinLocationIconOnline;
  late BitmapDescriptor pinLocationIconOffline;


  void _onMapCreated(GoogleMapController _cntlr) async {
    mapController = _cntlr;
  }
  void setCustomMapPinShop() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/destination_map_marker_ss.png');
    setState(() {
      _markers.add(
          Marker(
            markerId:const MarkerId('33333333'),
            position: pinPosition,
            icon: pinLocationIcon,
            infoWindow:InfoWindow(
              title: widget.addresses,
              snippet:widget.addresses,
            ),
          )
      );
    });
  }

  Future<List<AttendanceDetail>> attendance() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/attendance/${widget.id}'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<AttendanceDetail> productDetail = [];
      for (var jsonAttendance in jsonArray) {
        print(response.body);
        AttendanceDetail product = AttendanceDetail(
            attendanceStatus:jsonAttendance['attendance_status'],
            attendanceLatlong: g.convertModelString(jsonAttendance['attendance_latlong']),
            attendanceAddress:g.convertModelString(jsonAttendance['attendance_address']),
            driverName:g.convertModelString(jsonAttendance['driver_name']),
            updatedAt:g.convertModelString(jsonAttendance['updated_at']),
            createdAt: g.convertModelString(jsonAttendance['created_at']),
            shopId:jsonAttendance['shop_id'],
            attendanceDate: g.convertModelString(jsonAttendance['attendance_date']),
            id: jsonAttendance['id'],
            longitude: g.convertModelString(jsonAttendance['Longitude']),
            latitude:g.convertModelString(jsonAttendance['Latitude'])
        );
        productDetail.add(product);
      }
      return productDetail;
    } else {
      throw Exception('Failed to load album');
    }
  }
  void fetchLocation() async {
    final double lat = double.parse(widget.lat);
    final double lng = double.parse(widget.lng);
    _markers == null;
    g.coAddMarker(
        coLatLng: LatLng(lat,lng),
        coID: '11111111',
        coTitle: "location".translate().toFirstUppercase(),
        coDescription: widget.addresses,
        coPinLocationIcon: widget.status == 0 ? pinLocationIconOnline : pinLocationIconOffline
    ).then((markers){
      _markers = {};
      _markers.add(markers);
      alreadyLoading = true;
      setState(() {
      });
      CameraPosition updateCamera = CameraPosition(target: LatLng(lat, lng), zoom: 17);
      Duration _duration = const Duration(seconds: 1);
      Timer(_duration, (){
        CameraUpdate u2 = CameraUpdate.newCameraPosition(updateCamera);
        mapController.animateCamera(u2).then((value){
          var _duration = const Duration(seconds: 1);
          Timer(_duration, (){
            mapController.showMarkerInfoWindow(MarkerId('11111111'.toString()));
          });
        });
      });
    });
  }
  void _onLoading(){
    Duration duration =const Duration(seconds: 1);
    Timer(duration, fetchLocation);
  }

  @override
  void initState() {
    super.initState();
    BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/destination_map_marker_ss.png').then((onValue) {
      pinLocationIconOnline = onValue;
      BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/image/destination_map_marker_s.png').then((onValue) {
        pinLocationIconOffline = onValue;
        SchedulerBinding.instance.addPostFrameCallback((_) => _onLoading());

      });
    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Theme.of(context).primaryColor,
        brightness: Brightness.light,
        title: Text('attendance_detail'.translate().toFirstUppercaseWord()),
      ),
      body:FutureBuilder<List<AttendanceDetail>>(
        future: attendance(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            print(snapshot.hasData);
            List<AttendanceDetail> attendanceDetail = snapshot.data!;
            print(attendanceDetail.length.toString());
            return Stack(
              children: List.generate(attendanceDetail.length, (index) {
                return Column(
             children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    margin:const EdgeInsets.symmetric(horizontal: 10,vertical:  10),
                     child:(attendanceDetail[index].attendanceStatus == 0) ? Container(
                       padding: const EdgeInsets.symmetric(horizontal: 25,vertical:  3),
                       decoration: BoxDecoration(
                         color: Colors.green,
                         borderRadius: BorderRadius.circular(25),),
                       child:  Text('in'.translate().toFirstUppercase(), style:const TextStyle(
                         color: Colors.white
                       ),),
                     ):Container(
                       padding:const EdgeInsets.symmetric(horizontal: 20,vertical:  3),
                       decoration: BoxDecoration(
                         color: Colors.red,
                         borderRadius: BorderRadius.circular(25),),
                       child: Text('out'.translate().toFirstUppercase(), style:const TextStyle(
                         color: Colors.white
                       ),),
                     )
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal:10),
                        child: Text(
                          attendanceDetail[index].driverName,
                          style:const TextStyle(
                              fontSize: 17,
                              color: Colors.blue,
                              fontWeight: FontWeight.bold),
                           ),
                        ),
                     ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      child:Text(
                        g.stringDateTimeToFormatDay(attendanceDetail[index].attendanceDate),
                        style:const TextStyle(
                            fontSize: 16,
                            color: Colors.black
                        ),
                      ),
                    ),
                      const  SizedBox(
                        height: 5,
                      ),
                      Container(
                       padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: Text(
                         g.stringDateTimeToFormatTimes(attendanceDetail[index].createdAt),
                          style: const TextStyle(
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Row(
                  children: [
                    Icon(Icons.location_on_outlined, color: Colors.grey[500],),
                    Container(
                      width: MediaQuery.of(context).size.width/2,
                      padding:const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        attendanceDetail[index].attendanceAddress,
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.grey[700]
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const  SizedBox(
                height: 10,
              )
            ],
          ),
          _markers != null ? Expanded(child: GoogleMap(
          markers: _markers,
          mapType: MapType.normal,
          onMapCreated: _onMapCreated,
              myLocationEnabled: true,
          initialCameraPosition: const CameraPosition(
          target: _center,
          zoom: 11.0,
          )
          )):Center(
                child: Container(
                margin: const EdgeInsets.symmetric(vertical: 20),
                height: 40,
                width: 40,
                child: const CircularProgressIndicator()
                   ),
                ),
              ],
          );
              }),
            );
          } else if (snapshot.hasError) {
            print(snapshot.error);
            return Text('${snapshot.error}');
                }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      )

    );
  }
}