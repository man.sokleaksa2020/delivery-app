import 'package:flutter/material.dart';
import 'package:delivery_app/Screen/delivery/delivery.dart';
import 'package:delivery_app/helper/stringExtension.dart';
class NotificationScreen extends StatefulWidget {
  bool emplyLeading;
   NotificationScreen({Key? key, this.emplyLeading = true}) : super(key: key);

  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  List<IconBottomWidgetModel> getIconBottomWidget() {
    List<IconBottomWidgetModel> iconList = [];
    iconList.add(IconBottomWidgetModel(
        promotion: 'New Order from LUNA Coffee&Bakery',
        description: 'of Asain  Food from 15 to 20 may',
        date: 'Today',
        time: '9:05am',
        imageAssets: 'assets/image/logo.png',
        tap: () {
         Navigator.push(context, MaterialPageRoute(builder: (context)=>Delivery(id: '',)));
        }));
    iconList.add(IconBottomWidgetModel(
        promotion: 'New Order from LUNA Coffee&Bakery ',
        description: 'of Asain  Food from 15 to 20 may',
        date: 'Today',
        time: '9:05am',
        imageAssets: 'assets/image/logo.png',
        tap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Delivery(id: '',)));
        }));
    iconList.add(IconBottomWidgetModel(
        promotion:  'New Order from LUNA Coffee&Bakery ',
        description: 'of Asain  Food from 15 to 20 may',
        date: 'Today',
        time: '9:05am',
        imageAssets: 'assets/image/logo.png',
        tap: () {}));
    iconList.add(IconBottomWidgetModel(
        promotion:  'New Order from LUNA Coffee&Bakery ',
        description: 'of Asain  Food from 15 to 20 may',
        date: 'Today',
        time: '9:05am',
        imageAssets: 'assets/image/logo.png',
        tap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Delivery(id: '',)));
        }));
    iconList.add(IconBottomWidgetModel(
        promotion:  'New Order from LUNA Coffee&Bakery ',
        description: 'of Asain  Food from 15 to 20 may',
        date: 'Today',
        time: '9:05am',
        imageAssets: 'assets/image/logo.png',
        tap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=>Delivery(id: '',)));
        }));
    iconList.add(IconBottomWidgetModel(
        promotion: 'New Order from LUNA Coffee&Bakery ',
        description: 'of Asain  Food from 15 to 20 may',
        date: 'Today',
        time: '9:05am',
        imageAssets: 'assets/image/logo.png',
        tap: () {
         Navigator.push(context, MaterialPageRoute(builder: (context)=>Delivery(id: '',)));
        }));
    iconList.add(IconBottomWidgetModel(
        promotion:  'New Order from LUNA Coffee&Bakery ',
        description: 'of Asain  Food from 15 to 20 may',
        date: 'Today',
        time: '9:05am',
        imageAssets: 'assets/image/logo.png',
        tap: () {
         Navigator.push(context, MaterialPageRoute(builder: (context)=>Delivery(id: '',)));
        }));

    return iconList;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        brightness: Brightness.dark,
        elevation: 0.0,
        title:  Text("notification".translate().toFirstUppercase(), style:const TextStyle(
            color: Colors.white
        ),),
      ),
      body: Container(
        color: Colors.grey[200],
        padding: const EdgeInsets.symmetric(vertical: 10,horizontal: 10),
        child: ListView(
          children: List.generate(getIconBottomWidget().length, (index) {
            return InkWell(
              onTap: () {
                getIconBottomWidget()[index].tap();
              },
              child: Container(
                  margin: const EdgeInsets.symmetric(vertical: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context)
                              .hintColor
                              .withOpacity(0.15),
                          offset:const Offset(2, 4),
                          blurRadius: 10),
                       ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 100,
                        height:100,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Image.asset(
                            getIconBottomWidget()[index].imageAssets,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: MediaQuery.of(context).size.width/2,
                              child:  Text(
                                getIconBottomWidget()[index].promotion,
                                style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width/2,
                              child: Text(
                                getIconBottomWidget()[index].description,
                                style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey[600]),
                              ),
                            ),
                          const SizedBox(
                              height:5,
                            ),
                            Text(
                              getIconBottomWidget()[index].date,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey[600]),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              getIconBottomWidget()[index].time,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey[600]),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ),
              );
            }
          ),
        ),
      ),
    );
  }
}
class IconBottomWidgetModel {
  String imageAssets;
  String promotion;
  String description;
  String date;
  String time;
  GestureTapCallback tap;
  IconBottomWidgetModel({
    required this.imageAssets,
    required this.time,
    required this.tap,
    required this.date,
    required this.description,
    required this.promotion});
}