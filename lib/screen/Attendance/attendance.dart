import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/model/shop_model.dart' as locations;
import 'package:http/http.dart' as http;
import '../../model/last_status.dart';
import '../../utils/g_widget.dart';
import 'package:delivery_app/helper/stringExtension.dart';

class CheckInOut extends StatefulWidget {
  String  address = '' ;
  late String latLong = '' ;
  late String longlat = '';
  late int statusIn = 0;
  late int statusOut = 1;
  late double elat = 104.89206372631551;


  @override
  _CheckInOutState createState() => _CheckInOutState();

}
class _CheckInOutState extends State<CheckInOut> {
  late GoogleMapController mapController;
  late Set<Marker> _markers= {};
  static const LatLng _center =  LatLng(11.559858661399863, 104.92688361721004);
  late Set<Circle> _circles = {};
  bool coError = false;
  bool alreadyLoadingInOrOut = true;
  bool alreadyLoading = false;
  bool error = false;
  late LatLng latlong ;
  final Map<String, Marker> _marker = {};
  late final CameraPosition _cameraPosition;
  late BitmapDescriptor pinLocationIconOnline;
  late BitmapDescriptor pinLocationIconCompany;
  String typeInOut = '';
  late double matter = 100;
  Future<Position> _getGeoLocationPosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }
  getLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    print(position.latitude);
    setState(() {
      latlong= LatLng(position.latitude, position.longitude);
      _cameraPosition=CameraPosition(target:latlong,zoom:14.0 );
      mapController.animateCamera(
          CameraUpdate.newCameraPosition(_cameraPosition));
    });
  }
  Future<void> GetAddressFromLatLong(Position position)async {
    List<Placemark> placemarks = await placemarkFromCoordinates(position.latitude, position.longitude);
    widget.address = '${placemarks[1].street}, ${placemarks[2].name}, ${placemarks[2].subLocality},${placemarks[2].locality}';
    widget.latLong = '${position.latitude}, ${position.longitude}';
    setState(()  {
    });
  }
  void fetchLocation(){
    setState(() {
      _markers == null;
    });
    _getGeoLocationPosition().then((Position position)  async{
      setState(() {
        latlong= LatLng(position.latitude, position.longitude);
        _cameraPosition=CameraPosition(target:latlong,zoom:18.0 );
        mapController.animateCamera(
            CameraUpdate.newCameraPosition(_cameraPosition));
      });
        final googleOffices = await locations.getGoogleOffices();
          for (final office in googleOffices.result) {
          double startLat = double.parse(office.latitude.toString());
          double endLat = double.parse(position.latitude.toString());
          double startLong = double.parse(office.longitude.toString());
          double endLong = double.parse(position.longitude.toString());
          double distanceInMeters = Geolocator.distanceBetween(startLat,startLong,endLat,endLong);
          print('distanceInMeters==========');
          print(distanceInMeters);
          if(distanceInMeters > 100){
            setState(() {
              coError = true;
            });
          } else {
            LatLng _coCenter =  LatLng(startLat, startLong);
            _circles = {};
            _circles.add(Circle(
                circleId: const CircleId('11111111'),
                center: _coCenter,
                radius: 100,
                fillColor: Colors.blue.withOpacity(0.2),
                strokeWidth: 2,
                strokeColor: Colors.blueAccent));
                 _markers = {};
              }
           }
        if(coError == false){
          placemarkFromCoordinates(position.latitude, position.longitude).then((List<Placemark> placemarks){
            widget.address ='${placemarks[1].street}, ${placemarks[2].name}, ${placemarks[2].subLocality},${placemarks[2].locality}';
            widget.latLong = '${position.latitude}, ${position.longitude}';
            g.coAddMarker(
              coLatLng: LatLng(position.latitude, position.longitude),
              coID: '22222222',
              coTitle: "my location",
              coDescription: widget.address,
              coPinLocationIcon: pinLocationIconOnline,
            ).then((markers){
              _markers.add(markers);
              CameraPosition updateCamera = CameraPosition(target: LatLng(position.latitude, position.longitude), zoom: 18);
              Duration _duration =const Duration(seconds: 1);
              Timer(_duration, (){
                CameraUpdate u2 = CameraUpdate.newCameraPosition(updateCamera);
                mapController.animateCamera(u2).then((value){
                  var _duration =  const Duration(seconds: 2);
                 Timer(_duration, (){
                    mapController.showMarkerInfoWindow(MarkerId('11111111'.toString()));
                  });
                });
                alreadyLoading = true;
              });
              setState(() {
              });
            });
          }
          );
        }
      }).catchError((e){
        print('=======================================${e.toString()}');
        });
    }
  Future<List<Data>> attendanceStatus() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/attendance/get_last_status'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<Data> productDetail = [];
      for (var jsonAttendance in jsonArray) {
        Data product = Data(
          attendanceStatus: jsonAttendance['attendance_status'] ?? 1 ,
        );
        productDetail.add(product);
      }
      return productDetail;
    } else {
      throw Exception('Failed to load album');
    }
  }
  Future<http.Response> checkIn( String address,String latlong, int status, ) {
    return http.post(
      Uri.parse('http://172.26.16.113:81/api/attendance'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, dynamic>{
        'attendance_address':address,
        'attendance_latlong':latlong,
        'attendance_status':status,
      }),
    );
  }
  Future<http.Response> checkOut( String address,String latlong, int status, ) {
    return http.post(
      Uri.parse('http://172.26.16.113:81/api/attendance'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, dynamic>{
        'attendance_address':address,
        'attendance_latlong':latlong,
        'attendance_status':status,
      }),
    );
  }
  void _onMapCreated(GoogleMapController _cntlr) async
  {
    mapController = _cntlr;
    _cntlr.setMapStyle(Utils.mapStyle);
  }
  @override
  void initState() {
    super.initState();
    BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/destination_map_marker_ss.png').then((onValue) {
      pinLocationIconOnline = onValue;
      BitmapDescriptor.fromAssetImage(
          const ImageConfiguration(devicePixelRatio: 2.5),
          'assets/image/companyIcon.png').then((onValue) {
        pinLocationIconCompany = onValue;
        fetchLocation();
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        brightness:Brightness.dark ,
        elevation: 0.0,
        title:  Text('attendance'.translate().toFirstUppercaseWord()),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Positioned(
              child: GoogleMap(
                  markers: _markers,
                  circles: _circles,
                  mapType: MapType.normal,
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: const CameraPosition(
                    target: _center,
                    zoom:18.0,
                  )
               ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10,left: 20,right: 20,bottom: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15,bottom: 15),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Colors.blue,
                      ),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              const Icon(Icons.location_on,color: Colors.white,),
                              const SizedBox(
                                width: 8,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                     Text(
                                      'location'.translate().toFirstUppercase(),
                                      style: const TextStyle(fontSize: 15,color: Colors.white),
                                    ),
                                    const SizedBox(height: 10),
                                   widget.latLong != null ?
                                    Text(widget.address,
                                        style: const TextStyle(color: Colors.white,fontSize: 15)): const Text(''),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                            ],
                          ),
                        ],
                      )),
                ],
              ),
            ),
           coError == true ?
           Positioned(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  color: Colors.white,
                  child: Center(
                    child: SizedBox(
                      height: 400,
                      child: Column(
                        children:  [
                         const Icon(Icons.location_off_outlined, size: 60,color: Colors.redAccent,),
                          const SizedBox(
                            height: 10,
                          ),
                           Text('error'.translate().toFirstUppercase()+ '!', style:const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                          const SizedBox(height: 7),
                          Text('please_check_your_current_location_again'.translate().toFirstUppercase()+'\n'+'you_are_far_from_company_more_than_100_meter'.translate().toFirstUppercase(), textAlign: TextAlign.center,style:const TextStyle(
                              fontSize: 15
                          ),),
                          const   SizedBox(height: 15),
                        ],
                      ),
                    ),
                  ),
                )
            ):  _markers == null  ? Positioned(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  color: Colors.white,
                  child: Center(
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height/1 ,
                      child: Column(
                        children:  [
                          Text('please_wait'.translate().toFirstUppercase(), style:const TextStyle(fontWeight: FontWeight.bold),),
                          const SizedBox(height: 7),
                          Text('getting_your_current_location'.translate().toFirstUppercase()),
                          const SizedBox(height: 15),
                          const CircularProgressIndicator(),
                        ],
                      ),
                    ),
                  ),
                )
            ):
           Positioned(
             bottom: 0,
               left: 0,
               right: 0,
               child: Stack(
             children: [
               FutureBuilder<List<Data>>(
                 future: attendanceStatus(),
                 builder: (context, snapshot) {
                   if (snapshot.hasData) {
                     List<Data> attendance = snapshot.data!;
                     return  Stack(
                         children: List.generate(attendance.length, (index) {
                           return Stack(
                             children: [
                               _markers != '' ? Container(
                                   child: alreadyLoadingInOrOut ? LayoutBuilder(builder: (context, constraints) {
                                     return attendance[index].attendanceStatus == 1 ?Container(
                                       width: MediaQuery.of(context).size.width,
                                       color: Colors.green,
                                       height: 60,
                                       child: TextButton(
                                           onPressed: (){
                                             checkIn(widget.address, widget.latLong, widget.statusIn);

                                             Navigator.pop(context);
                                           }, child:  Text('check_in'.translate().toFirstUppercaseWord(),
                                         style:const TextStyle(
                                             color: Colors.white,
                                             fontSize: 23
                                         ),
                                       )),
                                     ) :
                                     Container(
                                       width: MediaQuery.of(context).size.width,
                                       color: Colors.red,
                                       height: 60,
                                       child:   TextButton(onPressed: (){
                                         checkOut(widget.address,widget.latLong, widget.statusOut);
                                         Navigator.pop(context);
                                       }, child: Text('check_out'.translate().toFirstUppercaseWord(),
                                         style:const TextStyle(
                                             color: Colors.white,
                                             fontSize: 23
                                         ),
                                       )),
                                     ); // create function here to adapt to the parent widget's constraints
                                   }) :
                                   const Center(
                                     child: SizedBox(
                                         height: 30,
                                         width: 30,
                                         child: CircularProgressIndicator()
                                     ),
                                   )
                               ) : Container()
                             ],
                           );
                         })
                     );}
                   else if (snapshot.hasError) {
                     return Text('${snapshot.error}');
                   }
                   return const Center(child: CircularProgressIndicator());
                 }
             )],
           ))
          ],
        ),
      ),
    );
  }
}
class Utils{
  static String mapStyle = '''
 [
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ebe3cd"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#523735"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f1e6"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#c9b2a6"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#dcd2be"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#ae9e90"
      }
    ]
  },
  {
    "featureType": "landscape.natural",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#93817c"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#a5b076"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#447530"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f1e6"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#fdfcf8"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f8c967"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#e9bc62"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e98d58"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#db8555"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#806b63"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8f7d77"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#ebe3cd"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dfd2ae"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#b9d3c2"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#92998d"
      }
    ]
  }
]
  ''';
}

