import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:delivery_app/Screen/AttendanceList/attendanceList.dart';
import 'package:delivery_app/Screen/DeliveryList/daliverylist.dart';
import 'package:delivery_app/Screen/Notification/notification.dart';
import 'package:delivery_app/Screen/delivery/delivery.dart';
import 'package:delivery_app/Screen/profile/profile.dart';
import 'package:delivery_app/model/user_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/helper/stringExtension.dart';
import '../../model/last_status.dart';
import '../Attendance/attendance.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);
  DateTime now = DateTime.now();
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String formattedDate = DateFormat('EEE dd MMM yyyy').format(now);
  final DateFormat formatter = DateFormat('MM/dd/yyyy');
  static DateTime get now =>  DateTime.now();
  String formattedTime = DateFormat('kk:mm:ss a').format(now);
  String dateTimeToFormatTimeWithSecond(DateTime now){
    var outputFormat = DateFormat('hh:mm a');
    return outputFormat.format(now);
  }

  List<IconTopWidgetModel> getIconTopWidget() {
    List<IconTopWidgetModel> iconList = [];
    iconList.add(IconTopWidgetModel(
      title: 'attendance'.translate(),
      imageAssets: 'assets/image/pin.png',
      tap: () {
        Navigator.of(context).pushNamed('/attendance', );
      },
    ));
    iconList.add(IconTopWidgetModel(
        title: 'attendance_lists'.translate().toFirstUppercaseWord(),
        imageAssets: 'assets/image/attendance-icon-13.png',
        tap: () {
          Navigator.of(context).pushNamed('/attendanceLists');
        }));
    iconList.add(IconTopWidgetModel(
        title: 'deliveries'.translate(),
        imageAssets: 'assets/image/png-transparent-doorstep-delivery-computer-icons-others-miscellaneous-silhouette-area.png',
        tap: () {
          Navigator.of(context).pushNamed('/delivery');
        }));
    iconList.add(IconTopWidgetModel(
        title: 'history_lists'.translate().toFirstUppercaseWord(),
        imageAssets: 'assets/image/order1.png',
        tap: () {
          Navigator.of(context).pushNamed('/deliveryList');
        }));
    return iconList;
  }
  List<UserModel> parseProducts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<UserModel>((json) => UserModel.fromJson(json)).toList();
  }
  Future<List<UserModel>> fetchAlbum() async {
    print('profile token'+g.userModel.token);
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/users'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      return parseProducts(response.body);
    } else {
      throw Exception('Failed to load album');
    }
  }
  Future<List<Data>> attendanceStatus() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/attendance/get_last_status'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];

      List<Data> productDetail = [];
      for (var jsonAttendance in jsonArray) {
        Data product = Data(
          attendanceStatus: jsonAttendance['attendance_status'] ?? 1 ,
        );
        productDetail.add(product);
      }
      return productDetail;
    } else {
      throw Exception('Failed to load album');
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber,
        appBar: AppBar(
            automaticallyImplyLeading: false,
            brightness: Brightness.dark,
            centerTitle: true,
            elevation: 0,
            backgroundColor: Colors.amber,
            leading: InkWell(
              child:Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(
                        Radius.circular(75.0),
                      )),
                  padding: const EdgeInsets.all(2),
                  width: 35.0,
                  height: 35.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child:(g.userModel.image != '')? Image.network('http://172.26.16.113:81/'+g.userModel.image, fit: BoxFit.cover,):
                    Image.asset('assets/image/Profile-PNG-Images.png'),
                  )
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Profile()));
              },
            ),
            title: LayoutBuilder(builder: (contextBuilder, size) {
              return Image.asset(
                'assets/image/turbotech.png',
                width: 100,
                height: 100,
              );
            }
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) =>  NotificationScreen(emplyLeading: false,)));
                },
                icon: const Icon(
                  Icons.notifications_none, color: Colors.white, size: 28,),
              )
            ]
        ),
        body: Stack(
        children: [
        Positioned(
        top: 65,
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0)),
                  color: Colors.white70
              ),
            )
        ),
        Column(
            children: <Widget>[
              const SizedBox(
                height: 15,
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Colors.blue
                  ),
                  padding: const EdgeInsets.only(
                      top: 15, right: 5, left: 5, bottom: 3),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment .spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.fromLTRB(
                              10, 0, 10, 55),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'hello'.translate(),
                                    style:const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white),
                                  ),
                                  const SizedBox(
                                    width: 5,
                                  ),
                                  Expanded(
                                      child: Text(g.userModel.name,
                                        style: const TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight
                                                .bold,
                                            color: Colors.white),
                                      )
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(formattedDate.translate().toFirstUppercaseWord(),
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15)),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(g.dateTimeToFormatTimeWithSecond(now).translate().toFirstUppercase(),
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                )
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Column(
                              children: [FutureBuilder<List<Data>>(
                                  future: attendanceStatus(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      List<Data> attendance = snapshot.data!;
                                      return Stack(
                                          children: List.generate(attendance.length, (index) {
                                         //   print(attendance[index].attendanceStatus);
                                            return Container(
                                              child: (attendance[index].attendanceStatus == 1 ) ? Container(
                                                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 3),
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(8),
                                                    color: Colors.red
                                                ),
                                                child:  Text(
                                                  'out'.translate().toFirstUppercase(),
                                                  style:const TextStyle(
                                                      color: Colors.white
                                                  ),),
                                              ) : Container(
                                                padding: const EdgeInsets
                                                    .symmetric(
                                                    horizontal: 20,
                                                    vertical: 3),
                                                decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(8),
                                                    color: Colors.green
                                                ),
                                                child:  Text(
                                                  'in'.translate().toFirstUppercase(),
                                                  style:const TextStyle(
                                                      color: Colors.white
                                                  ),),
                                              ),
                                            );
                                          }));
                                    } else if (snapshot.hasError) {
                                      return Text(
                                          '${snapshot.error}');
                                    }
                                    return const Center(
                                        child:
                                        CircularProgressIndicator());
                                  })
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                  child: Container(
                    color: Colors.white70,
                    padding: const EdgeInsets.all(15),
                    child: GridView.count(
                      crossAxisCount: 2,
                      shrinkWrap: true,
                      childAspectRatio: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      physics: const BouncingScrollPhysics(),
                      children: List.generate(
                          getIconTopWidget().length, (index) {
                        return InkWell(
                          onTap: () {
                            getIconTopWidget()[index].tap();
                          },
                          child: Stack(children: [
                            Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(5),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Theme
                                            .of(context)
                                            .hintColor
                                            .withOpacity(0.15),
                                        offset: const Offset(2, 4),
                                        blurRadius: 10),
                                  ],
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                                        width: MediaQuery.of(context).size.width/2,
                                        height: MediaQuery.of(context).size.height/15,
                                        child: ClipRRect(
                                          borderRadius: const BorderRadius
                                              .only(
                                              topLeft: Radius
                                                  .circular(5),
                                              topRight: Radius
                                                  .circular(
                                                  5)),
                                          child: Container(
                                            width: MediaQuery.of(context).size.width/13,
                                            height:MediaQuery.of(context).size.height/25,
                                            child: Image.asset(
                                              getIconTopWidget()[index].imageAssets,
                                              color: Theme
                                                  .of(context)
                                                  .primaryColor,
                                            ),
                                          )
                                        )
                                    ),
                                    Container(
                                      padding: const EdgeInsets
                                          .symmetric(
                                          horizontal: 10),
                                      child: Text(
                                        getIconTopWidget()[index]
                                            .title,
                                        style: const TextStyle(
                                          fontSize: 15,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ]
                          ),
                        );
                      }
                      ),
                    ),
                  )
              ),
            ]
        )
        ]),
    );
  }
}
class IconTopWidgetModel {
  String imageAssets;
  String title;
  GestureTapCallback tap;
  IconTopWidgetModel({
    required this.title,
    required this.imageAssets,
    required this.tap
  });
}

