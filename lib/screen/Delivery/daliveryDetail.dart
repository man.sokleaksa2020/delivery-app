import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/model/delivery_detail.dart';
import 'package:http/http.dart'as http;
import 'package:delivery_app/Screen/delivery/customer_delivery.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:location/location.dart' as loc;
import 'package:delivery_app/helper/stringExtension.dart';
class DeliveryDetail extends StatefulWidget {
 late  String id;
 late String status = '1';
 String statuses = 'delivered';
 DeliveryDetail({Key?  key, required this.id}) : super(key: key);

  @override
  _DeliveryDetailState createState() => _DeliveryDetailState();
}

class _DeliveryDetailState extends State<DeliveryDetail> {
  final loc.Location location = loc.Location();
  GoogleMapController? mapController;
  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPiKey = "AIzaSyBOXZ8ExfHJAN7vrX4fMqcL5AQy7TCK3ZE";
  final Set<Marker> _markers = {};
  Set<Marker> markers = Set(); //markers for google map
  Map<PolylineId, Polyline> polylines = {}; //polylines to show direction
  late BitmapDescriptor pinLocationIcon;
  late StreamSubscription<loc.LocationData> _locationSubscription;
  LatLng startLocation = const LatLng(11.576609335716572, 104.90601019536518);
  LatLng endLocation = const LatLng(11.620975249056924, 104.89216031042842);
  LatLng pinPositionlunaolapich = const LatLng(11.576609335716572, 104.90601019536518);

  var  icon = BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(24, 24)), 'assets/image/delivery.png');


  static const LatLng _center = LatLng(11.576609335716572, 104.90601019536518);
  bool alreadyLoading = false;
  bool error = false;
  LatLng pinPosition = const LatLng(11.582672483525592, 104.90029403637303);
  LatLng pinPositionUser = const LatLng(11.620975249056924, 104.89216031042842);
  LatLng pinPositionluna = const LatLng(11.573107897811683, 104.89943572955796);
  LatLng pinPositionlunatk = const LatLng(11.582630442106423, 104.9000580019989);
  late BitmapDescriptor pinLocationIconOffline;
  void _onMapCreated(GoogleMapController _cntlr) async {
    mapController = _cntlr;
    ///Here you have choose level of distance
  }
  void setCustomMapPintk() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/destination_map_marker_ss.png');
    setState(() {
      _markers.add(
          Marker(
            markerId: const MarkerId('44444444'),
            position: pinPositionlunaolapich,
            //icon: pinLocationIcon,
            infoWindow: const InfoWindow(
              title: 'LUNA Coffee & Bakery',
              snippet: '20 str 265 Toul Kork  Phnom Penh Cambodia',
            ),
          )
        );
    });
  }
  void setCustomMapPinShop() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/destination_map_marker_ss.png');
    setState(() {
      _markers.add(
          Marker(
            markerId: const MarkerId('33333333'),
            position: pinPosition,
            icon: pinLocationIcon,
            infoWindow: const InfoWindow(
              title: 'Vanda Location',
              snippet: '20 str 265 Toul Kork  Phnom Penh Cambodia',
            ),
          )
      );
    });
  }

  Future<List<DeliveryDetails>> deliverys() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/deliveries/${widget.id}'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<DeliveryDetails> productDetail = [];
      for (var jsonProducts in jsonArray) {
        DeliveryDetails product = DeliveryDetails(
            customerId:g.convertModelString(jsonProducts['customer_id']),
            customerAddress:g.convertModelString(jsonProducts['customer_address']),
            shopAddress: g.convertModelString(jsonProducts['shop_address']),
            productDetail:List<ProductDetail>.from(jsonProducts["product_detail"].map((x) => ProductDetail.fromJson(x))),
            paymentStatus: g.convertModelString(jsonProducts['payment_status']),
            totalPrice:g.convertModelString(jsonProducts['total_price']),
            shopPhone: g.convertModelString(jsonProducts['shop_phone']),
            createdAt: g.convertModelString(jsonProducts['created_at']),
            customerName:g.convertModelString(jsonProducts['customer_name']),
            shopName: g.convertModelString(jsonProducts['shop_name']),
            sellOrderId:g.convertModelString(jsonProducts['sell_order_id']),
            customerPhone:g.convertModelString(jsonProducts['customer_phone']),
            status:g.convertModelString(jsonProducts['status']),
        );
        productDetail.add(product);
      }
      return productDetail;
    }
    else {
      throw Exception('Failed to load album');
    }
  }
  Future<http.Response>  customerDelivery(String status) {
    return http.put(
      Uri.parse('http://172.26.16.113:81/api/deliveries/${widget.id}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, String>{
        'status':status,
      }),
    );
  }

  Future<void> _listenLocation(String status) async {
    _locationSubscription = location.onLocationChanged.handleError((onError) {
      print(onError);
      _locationSubscription.cancel();
      setState(() {
        _locationSubscription ;
      });
    }).listen((loc.LocationData currentlocation) async {
      await FirebaseFirestore.instance.collection('locationUser').doc('user').set({
        'latitude': currentlocation.latitude,
        'longitude': currentlocation.longitude,
        'status': status,

      }, SetOptions(merge: true));
    });
  }
  @override
  void initState() {
    super.initState();
    markers.add(Marker(
      markerId: MarkerId(startLocation.toString()),
      position: startLocation,
      infoWindow: const InfoWindow(
        title: 'your current location',
      ),
      icon:BitmapDescriptor.defaultMarker,
    ));
    markers.add(Marker(
      markerId: MarkerId(endLocation.toString()),
      position: endLocation,
      infoWindow: const InfoWindow(
        title: 'Luna Coffee&Bakery',
      ),
      icon: BitmapDescriptor.defaultMarker,
    ));
    getDirections();
    location.changeSettings(interval: 300, accuracy: loc.LocationAccuracy.high);
    location.enableBackgroundMode(enable: true);
  }
  getDirections() async {
    List<LatLng> polylineCoordinates = [];
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      googleAPiKey,
      PointLatLng(startLocation.latitude, startLocation.longitude),
      PointLatLng(endLocation.latitude, endLocation.longitude),
      travelMode: TravelMode.driving,
    );
    if (result.points.isNotEmpty) {
      for (var point in result.points) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      }
    } else {
      print(result.errorMessage);
    }
    addPolyLine(polylineCoordinates);
  }
  addPolyLine(List<LatLng> polylineCoordinates) {
    PolylineId id = const PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      color:Theme.of(context).primaryColor,
      points: polylineCoordinates,
      width: 8,
    );
    polylines[id] = polyline;
    setState(() {});
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          brightness: Brightness.dark ,
          elevation: 0.0,
          title: Text("delivery_detail".translate().toFirstUppercaseWord()),
        ),
        body:FutureBuilder<List<DeliveryDetails>>(
        future: deliverys(),
        builder: (context, snapshot) {
        if (snapshot.hasData) {
        List<DeliveryDetails> cartList = snapshot.data!;
        print(cartList.length.toString()+'hello');
         return Stack(
         children: List.generate(cartList.length, (index) {
         return Column(
          children: [
            Stack(children: [
               Column(
                 mainAxisAlignment: MainAxisAlignment.start,
                 crossAxisAlignment: CrossAxisAlignment.start,
                 children: [
                   const SizedBox(
                     height: 5,
                   ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     crossAxisAlignment: CrossAxisAlignment.end,
                     children: [
                       Container(
                           margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                           decoration: BoxDecoration(
                               borderRadius: BorderRadius.circular(15),
                               color: Theme.of(context).primaryColor
                           ),
                           child:(cartList[index].status =='Enable') ? Container(
                             padding: const EdgeInsets.symmetric(horizontal:15,vertical: 5),
                             child:  Text('preparing'.translate().toFirstUppercase(),style:const TextStyle(
                                 color: Colors.white
                             ), ),
                           ):
                           const Text('')
                       ),
                     ],
                   ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       Column(
                         mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Container(
                             padding: const EdgeInsets.symmetric(horizontal: 5),
                             child: Row(children: [
                               const Icon(Icons.home, color: Colors.grey,size: 23,),
                               Container(
                                 width: MediaQuery.of(context).size.width/2,
                                 padding: const EdgeInsets.symmetric(horizontal: 10),
                                 child: Text(
                                   cartList[index].shopName,
                                   style: const TextStyle(
                                       fontSize: 16,
                                       color: Colors.blueAccent,
                                       fontWeight: FontWeight.bold),
                                 ),
                               ),
                             ],),
                           ),
                           const SizedBox(
                             height: 5,
                           ),
                           Container(
                             width: MediaQuery.of(context).size.width/2,
                             padding: const EdgeInsets.symmetric(horizontal:20),
                             child: Text(
                              g.convertPhoneNumber(cartList[index].shopPhone) ,
                               style: TextStyle(
                                 fontSize: 16,
                                 color: Colors.grey[600],
                               ),
                             ),
                           ),
                         ],
                       ),
                       Column(
                         mainAxisAlignment: MainAxisAlignment.end,
                         crossAxisAlignment: CrossAxisAlignment.end,
                         children: [
                           Container(
                             padding: const EdgeInsets.symmetric(horizontal: 10),
                             child: Text(
                              g.stringDateTimeToFormatDays( cartList[index].createdAt),
                               style: const TextStyle(
                                   fontSize: 13,
                                   color: Colors.black
                               ),
                             ),
                           ),
                           const SizedBox(
                             height: 5,
                           ),
                           Container(
                             padding: const EdgeInsets.symmetric(horizontal: 10),
                             child: Text(
                               g.stringDateTimeToFormatTimes(cartList[index].createdAt),
                               style: const TextStyle(
                                 fontSize: 13,
                               ),
                             ),
                           ),
                           const SizedBox(
                             height: 5,
                           ),
                         ],
                       ),
                     ],
                   ),
                   const SizedBox(
                     height: 5,
                   ),
                   Container(
                     padding: const EdgeInsets.symmetric(horizontal:18),
                     child: Row(
                       children: [
                         Container(
                           width: MediaQuery.of(context).size.width/2,
                           padding: const EdgeInsets.symmetric(horizontal: 5),
                           child: Text(
                             cartList[index].shopAddress,
                             style: TextStyle(
                                 fontSize: 15,
                                 color: Colors.grey[600]
                             ),
                           ),
                         ),
                       ],
                     ),
                   ),
                   const SizedBox(
                     height: 10,
                   ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       Column(
                         mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Container(
                             padding: const EdgeInsets.symmetric(horizontal: 5),
                             child: Row(
                               children: [
                                 const Icon(Icons.person_rounded, color: Colors.grey,),
                                 Container(
                                   width: MediaQuery.of(context).size.width/2,
                                   padding: const EdgeInsets.symmetric(horizontal:5),
                                   child: Text(
                                     '${cartList[index].customerName},${g.convertPhoneNumber(cartList[index].customerPhone)}',
                                     style: const TextStyle(
                                         fontSize: 16,
                                         color: Colors.blueAccent,
                                         fontWeight: FontWeight.bold
                                     ),
                                   ),
                                 ),
                               ],
                             ),
                           ),
                           const SizedBox(height: 5,),
                           Container(
                             width: MediaQuery.of(context).size.width/1,
                             padding: const EdgeInsets.symmetric(horizontal: 20),
                             child: Column(
                               children:List.generate(cartList[index].productDetail.length, (value) {
                                 return Row(
                                   children: [
                                         Text(cartList[index].productDetail[value].productName,
                                           style: const TextStyle(
                                               color: Colors.red,
                                               fontSize: 15,
                                               fontWeight: FontWeight.bold
                                           ),
                                         ),
                                 SizedBox(
                                   width: MediaQuery.of(context).size.width/2,
                                   child: Row(
                                     children: [
                                       Text(' ( '+cartList[index].productDetail[value].size
                                       ),
                                       Text(', '
                                           + cartList[index].productDetail[value].sugar
                                       ),
                                       Container(
                                           child: (cartList[index].productDetail[value].cream == 1 )? const Text(','+'Cream'
                                           ):const Text(', '+'No cream')
                                       ),
                                       Text(',X'
                                           + cartList[index].productDetail[value].items,style: const TextStyle(
                                           color: Colors.red
                                       ),
                                       ),
                                       const Text(')'),
                                     ],
                                   ),
                                     )
                                   ],
                                 );
                               }
                               ),
                             ),
                           ),
                           const SizedBox(height: 5,),
                           Container(
                         width: MediaQuery.of(context).size.width/2,
                             padding: const EdgeInsets.symmetric(horizontal: 20),
                             child: Text(
                               cartList[index].customerAddress,
                               style: TextStyle(
                                   fontSize:15,
                                   color: Colors.grey[600]
                               ),
                             ),
                           ),
                         ],
                       ),
                     ],
                   ),
                   Column(
                     mainAxisAlignment: MainAxisAlignment.end,
                     crossAxisAlignment: CrossAxisAlignment.end,
                     children: [
                       Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         crossAxisAlignment: CrossAxisAlignment.end,
                         children: [
                           Container(
                             padding: const EdgeInsets.symmetric(horizontal: 10),
                             child: Text(
                               cartList[index].sellOrderId,
                               style: TextStyle(
                                   fontSize:15,
                                   color: Theme.of(context).primaryColor,
                                   fontWeight: FontWeight.bold
                               ),
                             ),
                           ),
                         ],
                       ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         crossAxisAlignment: CrossAxisAlignment.end,
                         children: [
                           Container(
                             padding: const EdgeInsets.symmetric(horizontal: 10),
                             child:(cartList[index].paymentStatus == 'Payments.caseOnDelivery')? const Text(
                               'Case on delivery',
                               style: TextStyle(
                                   fontSize:15,
                                   color: Colors.black,
                                   fontWeight: FontWeight.bold
                               ),
                             ):Text(
                               cartList[index].paymentStatus,
                               style: const TextStyle(
                                   fontSize:15,
                                   color: Colors.black,
                                   fontWeight: FontWeight.bold
                               ),
                             ),
                           ),
                         ],
                       ),
                       const SizedBox(
                         height: 5,
                       ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         crossAxisAlignment: CrossAxisAlignment.end,
                         children: [
                           Text('total_price'.translate().toFirstUppercaseWord()+':', style:const TextStyle(
                               fontWeight: FontWeight.bold,
                               fontSize: 15
                           ),),
                           Container(
                             padding: const EdgeInsets.symmetric(horizontal: 10),
                             child: Text('\$'
                                 '${cartList[index].totalPrice}',
                               style: const TextStyle(
                                   fontSize:16,
                                   color: Colors.red,
                                   fontWeight: FontWeight.bold
                               ),
                             ),
                           ),
                         ],
                       ),
                     ],
                   )
                 ],
               ),
             ]
             ),
            Expanded(
              child: Column(
                children: [
                Expanded(
                  child:GoogleMap(
                  scrollGesturesEnabled: true,
                  zoomGesturesEnabled: true,
                  initialCameraPosition: CameraPosition(
                    target: startLocation,
                    zoom: 12.5,
                  ),
                  markers:markers,
                  polylines: Set<Polyline>.of(polylines.values),
                  mapType: MapType.normal,
                  onMapCreated: (controller) {
                    setState(() {
                      mapController = controller;
                    });
                  },
                ),),
               Container(
                 padding:const EdgeInsets.symmetric(vertical:5, horizontal: 10),
                   child: TextButton(
                     child: Container(
                       padding: const EdgeInsets.symmetric(vertical:12  , horizontal:  30),
                       decoration: BoxDecoration(
                           borderRadius: BorderRadius.circular(10),
                           color: Colors.blueAccent
                       ),
                       child: Center(
                         child:  Text('delivery_to_customer'.translate().toFirstUppercaseWord(),
                           style:const TextStyle(
                               color: Colors.white,
                             fontSize: 20
                           ),),
                       ),
                     ),
                     onPressed: (){
                       _listenLocation(widget.statuses);
                       customerDelivery(widget.status);
                       Navigator.push(context, MaterialPageRoute(builder: (context)=>MapScreen(sellId:  cartList[index].sellOrderId,
                       )));
                     },
                   )
                  )
                ],
              )
            ),
          ],
        );
     })
    );
  }
        else if (snapshot.hasError) {
      return Text('${snapshot.error}');
    }
    return const Center(
        child: CircularProgressIndicator(
          color: Colors.amber,
        )
      );
    }
 )
    );
  }
}

