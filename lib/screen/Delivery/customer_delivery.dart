import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:delivery_app/model/delivery_detail.dart';
import 'package:delivery_app/tap.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:url_launcher/url_launcher.dart';
import 'package:delivery_app/helper/stringExtension.dart';
import 'package:location/location.dart' as loc;

class MapScreen extends StatefulWidget{
  late String sellId;
  late String completed = 'Enable';
  late String cancel = 'Disable';
  MapScreen({Key? key, required this.sellId}) : super(key: key);


  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  GoogleMapController? mapController;
  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPiKey = "AIzaSyBOXZ8ExfHJAN7vrX4fMqcL5AQy7TCK3ZE";
  final Set<Marker> _markers = {};
  Set<Marker> markers = Set();
  Map<PolylineId, Polyline> polylines = {};
  late BitmapDescriptor pinLocationIcon;
  LatLng startLocation = const LatLng(11.582672483525592, 104.90029403637303);
  LatLng endLocation = const LatLng(11.620975249056924, 104.89216031042842);
  LatLng pinPositionlunaolapich = const LatLng(11.576609335716572, 104.90601019536518);
   StreamSubscription<loc.LocationData>? _locationSubscription;

  var  icon = BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(size: Size(24, 24)), 'assets/image/delivery.png');
  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/delivery.png');
    setState(() {
      _markers.add(
          Marker(
            markerId: const MarkerId('22222222'),
            position: startLocation,
            icon: pinLocationIcon,
            infoWindow: const InfoWindow(
              title: 'LUNA Coffee & Bakery',
              snippet: '20 str 265 Toul Kork Phnom Penh Cambodia',
            ),
          )
      );
    });
  }
  _stopListening() {
    _locationSubscription?.cancel();
    setState(() {
      _locationSubscription == null;
    });
  }
  @override
  void initState(){
    super.initState();
    setCustomMapPin();
    markers.add(Marker(
      markerId: MarkerId(startLocation.toString()),
      position: startLocation,
      infoWindow: const InfoWindow(
        title: 'Starting Point ',
        snippet: 'Start Marker',
      ),
      icon:BitmapDescriptor.defaultMarker,
    ));
    markers.add(Marker(
      markerId: MarkerId(endLocation.toString()),
      position: endLocation,
      infoWindow: const InfoWindow(
        title: 'Destination Point ',
        snippet: 'Destination Marker',
      ),
      icon: BitmapDescriptor.defaultMarker,
    ));
    getDirections();
  }
  getDirections() async {
    List<LatLng> polylineCoordinates = [];
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      googleAPiKey,
      PointLatLng(startLocation.latitude, startLocation.longitude),
      PointLatLng(endLocation.latitude, endLocation.longitude),
      travelMode: TravelMode.driving,
    );
    if (result.points.isNotEmpty) {
      for (var point in result.points) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      }
    } else {
      print(result.errorMessage);
    }
    addPolyLine(polylineCoordinates);
  }
  addPolyLine(List<LatLng> polylineCoordinates) {
    PolylineId id = const PolylineId("poly");
    Polyline polyline = Polyline(
      polylineId: id,
      color:Theme.of(context).primaryColor,
      points: polylineCoordinates,
      width: 8,
    );
    polylines[id] = polyline;
    setState(() {});
  }
  Future<List<DeliveryDetails>> deliverys() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/deliveries/${widget.sellId}'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<DeliveryDetails> productDetail = [];
      for (var jsonProducts in jsonArray) {
        DeliveryDetails product = DeliveryDetails(
          customerId: g.convertModelString(jsonProducts['customer_id']),
          customerAddress: g.convertModelString(
              jsonProducts['customer_address']),
          shopAddress: g.convertModelString(jsonProducts['shop_address']),
          productDetail: List<ProductDetail>.from(
              jsonProducts["product_detail"].map((x) =>
                  ProductDetail.fromJson(x))),
          paymentStatus: g.convertModelString(jsonProducts['payment_status']),
          totalPrice: g.convertModelString(jsonProducts['total_price']),
          shopPhone: g.convertModelString(jsonProducts['shop_phone']),
          createdAt: g.convertModelString(jsonProducts['created_at']),
          customerName: g.convertModelString(jsonProducts['customer_name']),
          shopName: g.convertModelString(jsonProducts['shop_name']),
          sellOrderId: g.convertModelString(jsonProducts['sell_order_id']),
          customerPhone: g.convertModelString(jsonProducts['customer_phone']),
          status: g.convertModelString(jsonProducts['status']),
        );
        productDetail.add(product);
      }
      return productDetail;
    }
    else {
      throw Exception('Failed to load album');
    }
  }
  _launchPhoneURL(String phoneNumber) async {
    String url = 'tel:' + phoneNumber;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  Future<http.Response> completed(String status) {
    return http.post(
      Uri.parse('http://172.26.16.113:81/api/deliveries/${widget.sellId}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, String>{
        'status':status,
      }),
    );
  }
  Future<http.Response> cancel(String status) {
    return http.post(
      Uri.parse('http://172.26.16.113:81/api/deliveries/${widget.sellId}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, String>{
        'status':status,
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Theme.of(context).primaryColor,
          title: Text('customer'.translate().toFirstUppercase()),
        ),
        body:FutureBuilder<List<DeliveryDetails>>(
        future: deliverys(),
    builder: (context, snapshot) {
    if (snapshot.hasData) {
    List<DeliveryDetails> cartList = snapshot.data!;
    print(cartList.length.toString()+'hello');
         return Stack(
         children: List.generate(cartList.length, (index) {
         return Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
              const  SizedBox(
                  height: 10,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical:0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Icon(Icons.person_rounded, color: Colors.grey,),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal:5),
                        child: Text(
                          cartList[index].customerName,
                          style:const TextStyle(
                              fontSize: 16,
                              color: Colors.blueAccent,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      InkWell(
                        child:  Container(
                          width: MediaQuery.of(context).size.width/2,
                          padding: const EdgeInsets.symmetric(horizontal:5),
                          child: Text(
                           '('+ 'call'.translate().toFirstUppercase()+':'+ g.convertPhoneNumber(cartList[index].customerPhone)+')',
                            style:const TextStyle(
                                fontSize: 16,
                                color: Colors.blueAccent,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                        onTap: (){
                          _launchPhoneURL( cartList[index].customerPhone);
                        },
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 5,),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 25),
                  child: Column(
                    children:List.generate(cartList[index].productDetail.length, (value) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(cartList[index].productDetail[value].productName,
                            style: const TextStyle(
                                color: Colors.red,
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                          Text(' ( '+cartList[index].productDetail[value].size
                          ),
                          Text(', '
                              + cartList[index].productDetail[value].sugar
                          ),
                          Container(
                              child: (cartList[index].productDetail[value].cream == 1)? const Text(','+'Cream'):
                              const Text(', '+'No cream')
                          ),
                          Text(',X'
                              + cartList[index].productDetail[value].items,style: const TextStyle(
                              color: Colors.red
                          ),
                          ),
                          const Text(')'),
                        ],
                      );
                    }),
                  ),
                ),
                const SizedBox(height: 5,),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child:SizedBox(
                  width: MediaQuery.of(context).size.width/2,
                    child: Text(
                      cartList[index].customerAddress,
                      style: TextStyle(
                          fontSize:15,
                          color: Colors.grey[600]
                      ),
                    ),
                  )
                ),
                const SizedBox(
                  height: 10,
                )
              ],
            ),
            Expanded(child: GoogleMap(
              zoomGesturesEnabled: true,
              initialCameraPosition: CameraPosition(
                target: startLocation,
                zoom: 13.0, //initial zoom level
              ),
              markers:markers, //markers to show on map
              polylines: Set<Polyline>.of(polylines.values),
              mapType: MapType.normal,
              onMapCreated: (controller) {
                setState(() {
                  mapController = controller;
                });
              },
            ),
            ),
            Container(
                padding:const EdgeInsets.symmetric(horizontal: 0, vertical:  5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      child: Container(
                        width: MediaQuery.of(context).size.width/2.5,
                        padding: const EdgeInsets.symmetric(vertical: 15  , horizontal: 40),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color:Colors.red
                        ),
                        child: Center(
                          child:  Text('cancel'.translate().toFirstUppercase(),
                            style:const TextStyle(
                                color: Colors.white,
                                fontSize: 20
                            ),
                          ),
                        ),
                      ),
                      onPressed: (){
                        _stopListening();
                        cancel(widget.cancel);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Tap()));
                      },
                    ),
                    TextButton(
                      child: Container(
                        width: MediaQuery.of(context).size.width/2.5,
                        padding: const EdgeInsets.symmetric(vertical:15 , horizontal:  30),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.blueAccent
                        ),
                        child: Center(
                          child:  Text('complete'.translate().toFirstUppercase(),
                            style:const TextStyle(
                                color: Colors.white,
                                fontSize: 20
                            ),),
                        ),
                      ),
                      onPressed: (){
                        _stopListening();
                        completed(widget.completed);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Tap()));
                      },
                  )
               ],
           )),
          ],
        );
     }));
   }
    else if (snapshot.hasError) {
      return Text('${snapshot.error}');
    }
    return const Center(
        child: CircularProgressIndicator(
        color: Colors.amber,
    ));
}
)
    );}}

