import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/model/delivery_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/Screen/delivery/daliveryDetail.dart';
import 'package:http/http.dart' as http;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shimmer/shimmer.dart';
import 'package:location/location.dart' as loc;
import 'package:delivery_app/helper/stringExtension.dart';

class Delivery extends StatefulWidget {
  String id;
  String reject = 'Disable';
  String accept = 'Enable';
  late int totalPages ;
  String status = 'preparing';

  Delivery({Key? key, required this.id}) : super(key: key);

  @override
  _DeliveryState createState() => _DeliveryState();
}

class _DeliveryState extends State<Delivery> {
  final _baseUrl = 'http://172.26.16.113:81/api/deliveries';
  final loc.Location location = loc.Location();
  late StreamSubscription<loc.LocationData> _locationSubscription;
  int _page = 0;
  int _limit = 100;
  bool _enabled = true;
  bool _hasNextPage = true;

  bool _isFirstLoadRunning = false;

  bool _isLoadMoreRunning = false;
  int currentPage = 1;

  late int totalPages;

  List<Data> passengers = [];

  final RefreshController refreshController =
      RefreshController(initialRefresh: true);

  Future<bool> getPassengerData({bool isRefresh = false}) async {
    if (isRefresh) {
      currentPage = 1;
    } else {
      if (currentPage > totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }

    final Uri uri = Uri.parse(
        "http://172.26.16.113:81/api/deliveries?page=$currentPage");

    final response = await http.get(uri,
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );

    if (response.statusCode == 200) {
      final result = passengersDataFromJson(response.body);

      if (isRefresh) {
        passengers = result.data;
      } else {
        passengers.addAll(result.data);
      }
      currentPage++;
      totalPages = result.lastPage;

      print(response.body);
      setState(() {});
      return true;
    } else {
      return false;
    }
  }

  List<Data> data = [];

  List<Data> parseProducts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Data>((json) => Data.fromJson(json)).toList();
  }

  void _firstLoad() async {
    setState(() {
      _isFirstLoadRunning = true;
    });
    try {
      final res = await http.get(
        Uri.parse("$_baseUrl?_page=$_page&_limit=$_limit"),
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
        },
      );
      setState(() {
        if (res.statusCode == 200) {
          var jsonData = json.decode(res.body);
          var jsonArray = jsonData['data'];
          List<Data> productDetail = [];
          for (var jsonProducts in jsonArray) {
            Data product = Data(
              customerId: g.convertModelString(jsonProducts['customer_id']),
              customerAddress: g.convertModelString(jsonProducts['customer_address']),
              shopAddress: g.convertModelString(jsonProducts['shop_address']),
              productDetail: List<ProductDetail>.from(jsonProducts["product_detail"].map((x) => ProductDetail.fromJson(x))),
              paymentStatus: g.convertModelString(jsonProducts['payment_status']),
              totalPrice: g.convertModelString(jsonProducts['total_price']),
              shopPhone: g.convertModelString(jsonProducts['shop_phone']),
              createdAt: g.convertModelString(jsonProducts['created_at']),
              customerName: g.convertModelString(jsonProducts['customer_name']),
              shopName: g.convertModelString(jsonProducts['shop_name']),
              sellOrderId: jsonProducts['sell_order_id'],
              customerPhone: g.convertModelString(jsonProducts['customer_phone']),
              status: g.convertModelString(jsonProducts['status']),
              driverId:g.convertModelString(jsonProducts['driver_id']),
            );
            productDetail.add(product);
          }
        }
      });
    } catch (err) {
      print(err);
      print('Something went wrong');
    }
    setState(() {
      _isFirstLoadRunning = false;
    });
  }

  List<Data> parseProduct(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Data>((json) => Data.fromJson(json)).toList();
  }

  Future<List<Data>> slide() async {
    var response =
        await http.get(Uri.parse('http://172.26.16.113:81/api/sliders'));
    if (response.statusCode == 200) {
      return parseProducts(response.body);
    } else {
      throw Exception('Failed to load album');
    }
  }

  late ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _firstLoad();
   // delivers();
    // _controller = ScrollController()..addListener(delivers);
  }

  @override
  void dispose() {
    // _controller.removeListener(delivers);
    super.dispose();
  }

  Future<List<Data>> delivery() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/deliveries'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<Data> productDetail = [];
      for (var jsonProducts in jsonArray) {
        Data product = Data(
          customerId: g.convertModelString(jsonProducts['customer_id']),
          customerAddress:
              g.convertModelString(jsonProducts['customer_address']),
          shopAddress: g.convertModelString(jsonProducts['shop_address']),
          productDetail: List<ProductDetail>.from(jsonProducts["product_detail"]
              .map((x) => ProductDetail.fromJson(x))),
          paymentStatus: g.convertModelString(jsonProducts['payment_status']),
          totalPrice: g.convertModelString(jsonProducts['total_price']),
          shopPhone: g.convertModelString(jsonProducts['shop_phone']),
          createdAt: g.convertModelString(jsonProducts['created_at']),
          customerName: g.convertModelString(jsonProducts['customer_name']),
          shopName: g.convertModelString(jsonProducts['shop_name']),
          sellOrderId: g.convertModelString(jsonProducts['sell_order_id']),
          customerPhone: g.convertModelString(jsonProducts['customer_phone']),
          status: g.convertModelString(jsonProducts['status'])
          , driverId: g.convertModelString(jsonProducts['driver_id']),
        );
        productDetail.add(product);
      }
      return productDetail;
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<http.Response> actionButton(String status, String sellId) {
    return http.post(
      Uri.parse('http://172.26.16.113:81/api/deliveries'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, String>{
        'status': status,
        'sell_order_id': sellId,
      }),
    );
  }
  Future<http.Response> customerDelivery(String status) {
    return http.post(
      Uri.parse('http://172.26.16.113:81/api/deliveries/${widget.id}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
      body: jsonEncode(<String, String>{
        'status': status,
      }),
    );
  }
  _getLocation(String name, String phone, String image, String status) async {
    try {
      final loc.LocationData _locationResult = await location.getLocation();
      await FirebaseFirestore.instance.collection('locationUser').doc('user').set({
        'latitude': _locationResult.latitude,
        'longitude': _locationResult.longitude,
        'phone': phone,
        'image': image,
        'name': name,
        'status': status
      }, SetOptions(merge: true));
    } catch (e) {
      print(e);
    }
  }
  Future<void> _listenLocation(String name, String phone, String image) async {
    _locationSubscription = location.onLocationChanged.handleError((onError) {
      print(onError);
      _locationSubscription.cancel();
      setState(() {
        _locationSubscription ;
      });
    }).listen((loc.LocationData currentlocation) async {
      await FirebaseFirestore.instance.collection('locationUser').doc('user').set({
        'latitude': currentlocation.latitude,
        'longitude': currentlocation.longitude,
        'phone': phone,
        'image': image,
        'name': name
      }, SetOptions(merge: true));
    });
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Stack(children: <Widget>[
      SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: _header(context),
      ),
      Positioned(
          bottom: -10,
          left: -130,
          child: _circularContainer(width * .7, Colors.transparent,
              borderColor: Colors.red.withOpacity(0.3))),
      Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            automaticallyImplyLeading: true,
            brightness: Brightness.dark,
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Column(
              children:  <Widget>[
                Text(
                  'deliveries'.translate().toFirstUppercase(),
                ),
              ],
            ),
          ),
          body: SmartRefresher(
            controller: refreshController,
            enablePullUp: true,
            onRefresh: () async {
              final result = await getPassengerData(isRefresh: true);
              if (result) {
                refreshController.refreshCompleted();
              } else {
                refreshController.loadFailed();
              }
            },
            onLoading: () async {
              final result = await getPassengerData();
              if (result) {
                refreshController.loadComplete();
              } else {
                refreshController.loadFailed();
              }
            },
            child: ListView.separated(
              itemBuilder: (context, index) {
             final passenger = passengers[index];
             return Container(
                 margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                 decoration: BoxDecoration(
                   color: Colors.white,
                   borderRadius: BorderRadius.circular(10),
                   boxShadow: [
                     BoxShadow(
                         color: Theme.of(context).hintColor.withOpacity(0.15),
                         offset: const Offset(2, 4),
                         blurRadius: 5),
                   ],
                 ),
                 child:(passenger.status == "Disable" && passenger.status == "Enable") ?Container():Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     const SizedBox(
                       height: 10,
                     ),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         Column(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             Container(
                               padding:const EdgeInsets.symmetric(horizontal: 5),
                               child: Row(children: [
                                 const Icon(Icons.home, color: Colors.grey,size: 23,),
                                 Container(
                                   width: MediaQuery.of(context).size.width/2,
                                   padding: const EdgeInsets.symmetric(horizontal: 10),
                                   child: Text(passenger.shopName,
                                     style:const TextStyle(
                                         fontSize: 16,
                                         color: Colors.blueAccent,
                                         fontWeight: FontWeight.bold),
                                   ),
                                 ),
                               ],),
                             ),
                             const SizedBox(
                               height: 5,
                             ),
                             Container(
                               width: MediaQuery.of(context).size.width/2,
                               padding: const EdgeInsets.symmetric(
                                   horizontal:39
                               ),
                               child: Text(
                                 g.convertPhoneNumber(passenger.shopPhone),
                                 style: const TextStyle(
                                   fontSize: 16,
                                   color: Colors.black87,
                                 ),
                               ),
                             ),
                           ],
                         ),
                         Column(
                           mainAxisAlignment: MainAxisAlignment.end,
                           crossAxisAlignment: CrossAxisAlignment.end,
                           children: [
                             Container(
                               padding: const EdgeInsets.symmetric(horizontal: 10),
                               child: Text(
                                 g.stringDateTimeToFormatDays(passenger.createdAt),
                                 style: const TextStyle(
                                     fontSize: 13,
                                     color: Colors.black,
                                     fontWeight: FontWeight.bold
                                 ),
                               ),
                             ),
                             const SizedBox(
                               height: 5,
                             ),
                             Container(
                               padding: const EdgeInsets.symmetric(horizontal: 10),
                               child: Text(
                                 g.stringDateTimeToFormatTimes(passenger.createdAt),
                                 style: const TextStyle(
                                   fontSize: 13,
                                 ),
                               ),
                             ),
                           ],
                         ),
                       ],
                     ),
                     Container(
                       padding: const EdgeInsets.symmetric(horizontal:30),
                       child: Row(
                         children: [
                           Container(
                             width: MediaQuery.of(context).size.width/2,
                             padding: const EdgeInsets.symmetric(horizontal: 10),
                             child: Text(
                               passenger.shopAddress,
                               style: const TextStyle(
                                   fontSize: 15,
                                   color: Colors.black
                               ),
                             ),
                           ),
                         ],
                       ),
                     ),
                     const SizedBox(height: 5,),
                     Container(
                       padding: const EdgeInsets.symmetric(horizontal: 5),
                       child: Row(
                         children: [
                           const Icon(Icons.person_rounded, color: Colors.grey,),
                           Container(

                             padding: const EdgeInsets.symmetric(horizontal:10),
                             child: Text(
                               '${passenger.customerName},${g.convertPhoneNumber(passenger.customerPhone)}',
                               style:const TextStyle(
                                   fontSize: 16,
                                   color: Colors.blueAccent,
                                   fontWeight: FontWeight.bold
                               ),
                             ),
                           ),
                         ],
                       ),
                     ),
                     Container(
                       width: MediaQuery.of(context).size.width/1,
                       padding: const EdgeInsets.symmetric(horizontal: 37),
                       child: Column(
                         children:List.generate(passenger.productDetail.length, (value) {
                           return Row(
                             mainAxisAlignment: MainAxisAlignment.start,
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [
                               Text(passenger.productDetail[value].productName,
                                 style: const TextStyle(
                                     color: Colors.red,
                                     fontSize: 15,
                                     fontWeight: FontWeight.bold
                                 ),
                               ),
                               Text(' ( '+passenger.productDetail[value].size
                               ),
                               Text(', '
                                   + passenger.productDetail[value].sugar
                               ),
                               Container(
                                   child: (passenger.productDetail[value].cream == 1 )? const Text(','+'Cream'
                                   ):const Text(', '+'No cream')
                               ),
                               Text(',X'
                                   + passenger.productDetail[value].items,style: const TextStyle(
                                   color: Colors.red
                               ),
                               ), const Text(')'
                               ),
                             ],
                           );
                         }),
                       ),
                     ),
                     const SizedBox(height: 5,),
                     Container(
                       width: MediaQuery.of(context).size.width/2,
                       padding: const EdgeInsets.symmetric(horizontal: 37),
                       child: Text(
                         passenger.customerAddress,
                         style: const TextStyle(
                             fontSize:15,
                             color: Colors.black87
                         ),
                       ),
                     ),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.end,
                       crossAxisAlignment: CrossAxisAlignment.end,
                       children: [
                         Container(
                           padding: const EdgeInsets.symmetric(horizontal: 10),
                           child: Text(
                             passenger.sellOrderId.toString(),
                             style:const TextStyle(
                                 fontSize:15,
                                 color: Colors.blue,
                                 fontWeight: FontWeight.bold
                             ),
                           ),
                         ),
                       ],
                     ),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.end,
                       crossAxisAlignment: CrossAxisAlignment.end,
                       children: [
                         Container(
                           padding: const EdgeInsets.symmetric(horizontal: 10),
                           child: Text(
                             passenger.paymentStatus,
                             style: const TextStyle(
                                 fontSize:16,
                                 color: Colors.black,
                                 fontWeight: FontWeight.bold
                             ),
                           ),
                         ),
                       ],
                     ),
                     const SizedBox(
                       height: 5,
                     ),
                     Row(
                       mainAxisAlignment: MainAxisAlignment.end,
                       crossAxisAlignment: CrossAxisAlignment.end,
                       children: [
                          Text('total_price'.translate().toFirstUppercaseWord()+' :', style:const TextStyle(
                             fontWeight: FontWeight.bold,
                             fontSize: 16
                         ),),
                         Container(
                           padding: const EdgeInsets.symmetric(horizontal: 10),
                           child: Text('\$'
                               '${passenger.totalPrice}',
                             style: const TextStyle(
                                 fontSize:16,
                                 color: Colors.red,
                                 fontWeight: FontWeight.bold
                             ),
                           ),
                         ),
                       ],
                     ),
                     Container(
                       padding: const EdgeInsets.symmetric(horizontal: 10),
                       child: const Divider(
                         color: Colors.grey,
                       ),
                     ),
                     Container(
                       margin: const EdgeInsets.symmetric(horizontal: 10),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.center,
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: [
                           TextButton(
                            // color:Colors.red,
                             onPressed: () async {
                               setState(() {
                                 actionButton(widget.reject, passenger.sellOrderId);
                               });
                             },
                             child: Container(
                               padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                               decoration: BoxDecoration(
                                 color: Colors.red,
                                 borderRadius: BorderRadius.circular(10),
                                 boxShadow: [
                                   BoxShadow(
                                       color: Theme.of(context).hintColor.withOpacity(0.15),
                                       offset: const Offset(2, 4),
                                       blurRadius: 5),
                                 ],
                               ),
                               child:   Text("reject".translate().toFirstUppercase(), style:const TextStyle(
                                   fontSize: 18,
                                 color: Colors.white
                               ),
                               ),
                             )
                           ),
                           const SizedBox(width: 10,),
                           TextButton(
                             onPressed: () async {
                               _getLocation(g.userModel.name,g.userModel.phone, g.userModel.image,widget.status );
                               print(g.userModel.name);
                               _listenLocation(g.userModel.name, g.userModel.phone, g.userModel.image);
                               actionButton(widget.accept, passenger.sellOrderId);
                               Navigator.push(context, MaterialPageRoute(builder: (context)=>DeliveryDetail(id:passenger.sellOrderId ,)));
                             }, child:  Container(
                             padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 8),
                             decoration: BoxDecoration(
                               color: Colors.blueAccent,
                               borderRadius: BorderRadius.circular(10),
                               boxShadow: [
                                 BoxShadow(
                                     color: Theme.of(context).hintColor.withOpacity(0.15),
                                     offset: const Offset(2, 4),
                                     blurRadius: 5),
                               ],
                             ),
                             child:  Text("accept".translate().toFirstUppercase(), style:const TextStyle(
                                 fontSize: 18,
                                 color: Colors.white
                             ),
                             ),
                           )
                           ),
                         ],
                       ),
                     ),
                     const SizedBox(
                       height: 15,
                     )
                   ],
                 )
             );},
              separatorBuilder: (context, index) => Container(),
              itemCount: passengers.length,
            ),
          )
          )
    ]);
  }
}

Widget _header(BuildContext context) {
  var width = MediaQuery.of(context).size.width;
  return Container(
    color: Colors.grey[200],
    child: Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
          child: Container(
              height: 180,
              width: width,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              child: Stack(
                fit: StackFit.expand,
                alignment: Alignment.center,
                children: <Widget>[
                  Positioned(
                      top: 30,
                      right: -100,
                      child: _circularContainer(300, Colors.amber)),
                  Positioned(
                      top: -180,
                      right: -30,
                      child: _circularContainer(width * .7, Colors.amber,
                          borderColor: Colors.amber)),
                  Positioned(
                      top: 40,
                      left: 0,
                      child: Container(
                          width: width,
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const <Widget>[
                              SizedBox(height: 10),
                              SizedBox(height: 20),
                            ],
                          )))
                ],
              )),
        ),
      ],
    ),
  );
}

Widget _circularContainer(double height, Color color,
    {Color borderColor = Colors.transparent, double borderWidth = 2}) {
  return Container(
    height: height,
    width: height,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: color,
      border: Border.all(color: borderColor, width: borderWidth),
    ),
  );
}
