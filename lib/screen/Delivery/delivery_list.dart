import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:delivery_app/model/delivery_model.dart';
import 'package:delivery_app/renderdata/delivery_render.dart';
import 'package:delivery_app/renderdata/renderdata.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/utils/g_widget.dart' as gWidget;
import 'package:delivery_app/utils/g.dart' as g;
import '../../renderdata/coscroll.dart';


class DeliveryList extends StatefulWidget {
  const DeliveryList({Key? key}) : super(key: key);

  @override
  DeliveryListState createState() => DeliveryListState();
}

class DeliveryListState extends State<DeliveryList> {
  RenderRowData coRenderRowData = DeliveryRenderData();
  late ScrollController _controller;
  late String status;
  late DateTime filterDate;
  TextEditingController reviewTextController = TextEditingController();

  Future _getMoreData({refresh: true}) async {
    Map<String, dynamic> fields = {
      'token' : g.userModel.token,
    };
    if(status != null){
      fields ['status']=status;
    }
    if(filterDate !=null){
      fields['date'] = gCo.formatDateOfDay(filterDate, format: gCo.formatDateSql);
    }
    if(refresh && coRenderRowData.dataRow.list != null){
      if(coRenderRowData.dataRow.list.length > 10){
        gCo.scrollToTop(_controller);
      }
    }
    coRenderRowData.getData(setState, refresh: refresh, fields: fields);
  }
  _scrollListener() {
    gCo.scrollRenderDataController(_controller, RenderRowData: coRenderRowData).then((value){
      _getMoreData(refresh: false);
    });
  }
  Future<List<Data>> delivery() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/deliveries'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<Data> productDetail = [];
      for (var jsonProducts in jsonArray) {
        Data product = Data(
            customerId:g.convertModelString(jsonProducts['customer_id']),
            customerAddress:g.convertModelString(jsonProducts['customer_address']),
            shopAddress: g.convertModelString(jsonProducts['shop_address']),
            productDetail:List<ProductDetail>.from(jsonProducts["product_detail"].map((x) => ProductDetail.fromJson(x))),
            paymentStatus: g.convertModelString(jsonProducts['payment_status']),
            totalPrice:g.convertModelString(jsonProducts['total_price']),
            shopPhone: g.convertModelString(jsonProducts['shop_phone']),
            createdAt: g.convertModelString(jsonProducts['created_at']),
            customerName:g.convertModelString(jsonProducts['customer_name']),
            shopName: g.convertModelString(jsonProducts['shop_name']),
            sellOrderId:g.convertModelString(jsonProducts['sell_order_id']),
            customerPhone:g.convertModelString(jsonProducts['customer_phone']),
            status: g.convertModelString(['status']),
            driverId:  g.convertModelString(['driver_id'])
        );
        productDetail.add(product);
      }
      return productDetail;
    }
    else {
      throw Exception('Failed to load album');
    }
  }
  @override
  void initState(){
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _getMoreData();
  }
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Expanded(
          child:CoScroll(
            onRefresh: (val) {
              _getMoreData(refresh: true);
            },
            scrollController: _controller,
            child: Column(
              children: <Widget>[
                coRenderRowData.dataRow.list != null
                    ? coRenderRowData.dataRow.list.length > 0
                    ? Container(
                    child: buildList(coRenderRowData.dataRow.list))
                    : gWidget.coZero()
                    : gWidget.coZero(),
                coRenderRowData.dataRow.noMore == false
                    ? Container()
                    : gWidget.coZero(),
              ],
            ),
          ),
        )
      ],
    );
  }
  buildList(list) {}
}
