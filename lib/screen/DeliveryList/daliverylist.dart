import 'dart:convert';
import 'dart:io';
import 'package:delivery_app/Screen/DeliveryList/deliveryListDetail.dart';
import 'package:delivery_app/model/history_model.dart';
import 'package:delivery_app/renderdata/formloading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:http/http.dart' as http;
import 'package:delivery_app/utils/g_form.dart' as gForm;
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shimmer/shimmer.dart';
import 'package:delivery_app/helper/stringExtension.dart';
class DeliveryList extends StatefulWidget {
  const DeliveryList({Key? key}) : super(key: key);

  @override
  _DeliveryListState createState() => _DeliveryListState();
}

class _DeliveryListState extends State<DeliveryList> {
  late DateTime filterDate = DateTime.now() ;
  int currentPage = 1;
  bool _enabled = true;
  late int totalPages;

  List<Data> passengers = [];

  final RefreshController refreshController =
  RefreshController(initialRefresh: true);

  Future<bool> getPassengerDatas({bool isRefresh = false,}) async {

    if (isRefresh) {
      currentPage = 1;
    } else {
      if (currentPage >= totalPages) {
        refreshController.loadNoData();
        return false;
      }
    }
    final Uri uri = Uri.parse(
        "http://172.26.16.113:81/api/deliveriesHistory?page=$currentPage&filter=${g.stringDateTimeToFormatDates(filterDate.toString())}");
    final response = await http.get(
      uri,
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      final result = passengersDataFromJson(response.body);

      if (isRefresh) {
        passengers = result.data;
      } else {
        passengers.addAll(result.data);
      }
      currentPage++;
      totalPages = result.lastPage;
      print(response.body);
      setState(() {});
      return true;
    } else {
      return false;
    }
  }

  Future<List<Data>> history() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/deliveriesHistory'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<Data> productDetail = [];
      for (var jsonProducts in jsonArray) {
        print(response.body);
        Data product = Data(
            customerId: g.convertModelString(jsonProducts['customer_id']),
            customerAddress: g.convertModelString(jsonProducts['customer_address']),
            shopAddress: g.convertModelString(jsonProducts['shop_address']),
            productDetail: List<ProductDetail>.from(jsonProducts["product_detail"].map((x) => ProductDetail.fromJson(x))),
            paymentStatus: g.convertModelString(jsonProducts['payment_status']),
            totalPrice: g.convertModelString(jsonProducts['total_price']),
            shopPhone: g.convertModelString(jsonProducts['shop_phone']),
            createdAt: g.convertModelString(jsonProducts['created_at']),
            customerName: g.convertModelString(jsonProducts['customer_name']),
            shopName: g.convertModelString(jsonProducts['shop_name']),
            sellOrderId: g.convertModelString(jsonProducts['sell_order_id']),
            customerPhone: g.convertModelString(jsonProducts['customer_phone']),
            status:jsonProducts['status'],);

        productDetail.add(product);
      }
      return productDetail;
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Stack(children: <Widget>[
      SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: _header(context),
      ),
      Positioned(
          bottom: -10,
          left: -130,
          child: _circularContainer(width * .7, Colors.transparent,
              borderColor: Colors.red.withOpacity(0.3))),
      Scaffold(
          // key: widget.scaffoldKey,
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            automaticallyImplyLeading: true,
            brightness: Brightness.dark,
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Column(
              children:  <Widget>[
                Text('history_lists'.translate().toFirstUppercaseWord()),
              ],
            ),
          ),
          body:Column(
    children: [
      Container(
          height: 30,
          margin: const EdgeInsets.symmetric(vertical: 20),
          child: ListView(
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              children: [
                const SizedBox(width: 10,),
                gForm.CoDate(
                  text: 'Filter Date',
                  initialDateTime: DateTime.now(),
                  dateFormatDisplay:  'd MMM, y',
                  onChanged: (v){
                    filterDate = v;
                    getPassengerDatas();
                  },
                  customBuilder: (context, val){
                    return Container(
                        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.blueAccent
                        ),
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                             Text('filter_date'.translate().toFirstUppercase(), style:const TextStyle(fontSize: 14 ,color: Colors.white, fontWeight: FontWeight.bold),),
                            const SizedBox(width: 5,),
                            filterDate != null ? Text(': ${g.stringDateTimeToFormatDay(filterDate.toString())}', style:const TextStyle(fontSize: 14 ,color: Colors.white),) : const SizedBox(width: 0, height: 0,),
                            const Icon(Icons.date_range, size: 16, color: Colors.white)
                          ],
                        )
                    );
                  },
                ),
                const SizedBox(
                  width: 10,
                ),
                const SizedBox(width: 10,),

              ])),
          Expanded(
          child: SmartRefresher(
          controller: refreshController,
          enablePullUp: true,
          onRefresh: () async {
            final result = await getPassengerDatas(isRefresh: true);
            if (result) {
              refreshController.refreshCompleted();
            } else {
            refreshController.refreshFailed();
            }
          },
          onLoading: () async {
            final result = await getPassengerDatas();
            if (result) {
              refreshController.loadComplete();
            } else {
              refreshController.loadFailed();
            }
          },
          child: ListView.separated(
            itemBuilder: (context, index) {
              final history = passengers[index];
              return GestureDetector(
                child:  Container(
                    margin: const EdgeInsets.symmetric(
                        vertical: 5, horizontal: 10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Theme.of(context)
                                .hintColor
                                .withOpacity(0.15),
                            offset: const Offset(2, 4),
                            blurRadius: 10),
                      ],
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                                padding:
                                const EdgeInsets.symmetric(
                                    horizontal: 10,
                                    vertical: 5),
                                child: (history.status == 'Disable')?Container(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 30),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(15),),
                                  child:  Text('failed'.translate().toFirstUppercase(),
                                    style:const TextStyle(
                                        color: Colors.white
                                    ),
                                  ),
                                ):Container(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.blueAccent,
                                    borderRadius: BorderRadius.circular(15),),
                                  child:  Text('completed'.translate().toFirstUppercase(), style:const TextStyle(
                                      color:Colors.white
                                  ),),
                                )
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment:
                              MainAxisAlignment.start,
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Container(
                                  padding:
                                  const EdgeInsets.symmetric(
                                      horizontal: 5),
                                  child: Row(
                                    children: [
                                      const Icon(
                                        Icons.home,
                                        color: Colors.grey,
                                        size: 23,
                                      ),
                                      Container(
                                        width:
                                        MediaQuery.of(context).size
                                            .width /
                                            2,
                                        padding: const EdgeInsets
                                            .symmetric(
                                            horizontal: 10),
                                        child: Text(
                                          history
                                              .shopName,
                                          style:const TextStyle(
                                              fontSize: 16,
                                              color: Colors.blue,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  width: MediaQuery.of(context)
                                      .size
                                      .width /
                                      2,
                                  padding:
                                  const EdgeInsets.symmetric(
                                      horizontal: 37),
                                  child: Text(
                                    g.convertPhoneNumber( history
                                        .shopPhone),
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.grey[600],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment:
                              MainAxisAlignment.end,
                              crossAxisAlignment:
                              CrossAxisAlignment.end,
                              children: [
                                Container(
                                  padding:
                                  const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Text(
                                    g.stringDateTimeToFormatDays(history.createdAt),
                                    style: const TextStyle(
                                        fontSize:13,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  padding:
                                  const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Text(
                                    g.stringDateTimeToFormatTimes(history.createdAt),
                                    style: const TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                              ],
                            ),
                          ],
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30),
                          child: Row(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                padding:
                                const EdgeInsets.symmetric(
                                    horizontal: 10),
                                child: Text(
                                  history.shopAddress,
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.grey[600]),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 5),
                          child: Row(
                            children: [
                              const Icon(
                                Icons.person_rounded,
                                color: Colors.grey,
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 2,
                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  history
                                      .customerName,
                                  style:const TextStyle(
                                      fontSize: 16,
                                      color: Colors.blue,
                                      fontWeight:
                                      FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          width:
                          MediaQuery.of(context).size.width / 2,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 37),
                          child: Text(
                            g.convertPhoneNumber( history.customerPhone),
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.grey[600]),
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 37),
                          child: Column(
                            children:List.generate(history.productDetail.length, (value) {
                              return Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(history.productDetail[value].productName,
                                    style: const TextStyle(
                                        color: Colors.red,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                  Text(' ( '+history.productDetail[value].size
                                  ),
                                  Text(', '
                                      + history.productDetail[value].sugar
                                  ),
                                  Container(
                                      child: (history.productDetail[value].cream == 1)? const Text(','+'Cream'
                                      ):const Text(', '+'No cream')
                                  ),
                                  Text(',X'
                                      + history.productDetail[value].items,style: const TextStyle(
                                      color: Colors.red
                                  ),
                                  ), const Text(')'
                                  ),
                                ],
                              );
                            }),
                          ),
                        ),
                        Container(
                          width:
                          MediaQuery.of(context).size.width /
                              2,
                          padding: const EdgeInsets.symmetric(
                              horizontal: 37),
                          child: Text(
                            history
                                .customerAddress,
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.grey[600]),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Container(
                              // width: MediaQuery.of(context).size.width/2,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10),
                              child: Text(
                                history.sellOrderId.toString(),
                                style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                             Text(
                              'total_price'.translate().toFirstUppercase()+':',
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10),
                              child: Text(
                                '\$'
                                    '${history.totalPrice}',
                                style: const TextStyle(
                                    fontSize: 16,
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        )
                      ],
                    )),
                onTap: (){
                 Navigator.push(context, MaterialPageRoute(builder: (context)=>DeliveryListDetail(sellId: history.sellOrderId,)));
                },
              );
            },
            separatorBuilder: (context, index) => Container(),
            itemCount: passengers.length,
            ),
          ),
         ),
      ])
      )
    ]);
  }
}
Widget _header(BuildContext context) {
  var width = MediaQuery.of(context).size.width;

  return Container(
    color: Colors.grey[200],
    child: Column(
      children: <Widget>[
        ClipRRect(
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
          child: Container(
              height: 250,
              width: width,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
              ),
              child: Stack(
                fit: StackFit.expand,
                alignment: Alignment.center,
                children: <Widget>[
               /*   Positioned(
                      top: 30,
                      right: -100,
                      child: _circularContainer(300, Colors.lightBlue)),*/
              /*    Positioned(
                      top: -180,
                      right: -30,
                      child: _circularContainer(width * .7, Colors.transparent,
                          borderColor: Colors.red)),*/
                  Positioned(
                      top: 40,
                      left: 0,
                      child: Container(
                          width: width,
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const <Widget>[
                              SizedBox(height: 10),
                              SizedBox(height: 20),
                            ],
                          )))
                ],
              )),
        ),
      ],
    ),
  );
}

Widget _circularContainer(double height, Color color,
    {Color borderColor = Colors.transparent, double borderWidth = 2}) {
  return Container(
    height: height,
    width: height,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      color: color,
      border: Border.all(color: borderColor, width: borderWidth),
    ),
  );
}
