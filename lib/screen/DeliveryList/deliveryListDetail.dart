import 'dart:convert';
import 'dart:ffi';
import 'dart:io';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/model/history_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:delivery_app/helper/stringExtension.dart';

class DeliveryListDetail extends StatefulWidget {
  late String sellId;
  DeliveryListDetail({Key? key, required this.sellId}) : super(key: key);

  @override
  _DeliveryListDetailState createState() => _DeliveryListDetailState();
}

class _DeliveryListDetailState extends State<DeliveryListDetail> {
  late GoogleMapController mapController;
  Set<Marker> _markers = {};
  static const LatLng _center = LatLng(11.576609335716572, 104.90601019536518);
  bool alreadyLoading = false;
  bool error = false;
  LatLng pinPosition = const LatLng(11.582672483525592, 104.90029403637303);
  LatLng pinPositionUser = const LatLng(11.620975249056924, 104.89216031042842);
  LatLng pinPositionluna = const LatLng(11.573107897811683, 104.89943572955796);
  LatLng pinPositionlunatk = const LatLng(11.582630442106423, 104.9000580019989);
  LatLng pinPositionlunaolapich = const LatLng(11.576609335716572, 104.90601019536518);
  late BitmapDescriptor pinLocationIcon;
  late BitmapDescriptor pinLocationIconOffline;

  void _onMapCreated(GoogleMapController _cntlr) async {
    mapController = _cntlr;
  }
  void setCustomMapPintk() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/destination_map_marker_ss.png');
    setState(() {
      _markers.add(Marker(
        markerId: const MarkerId('44444444'),
        position: pinPositionlunaolapich,
        //icon: pinLocationIcon,
        infoWindow: const InfoWindow(
          title: 'LUNA Coffee & Bakery',
          snippet: '20 str 265 Toul Kork  Phnom Penh Cambodia',
        ),
      ));
    });
  }
  void setCustomMapPinShop() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/destination_map_marker_ss.png');
    setState(() {
      _markers.add(Marker(
        markerId: const MarkerId('33333333'),
        position: pinPosition,
        icon: pinLocationIcon,
        infoWindow: const InfoWindow(
          title: 'Vanda Location',
          snippet: '20 str 265 Toul Kork  Phnom Penh Cambodia',
        ),
      ));
    });
  }
  Future<List<HistoryDetail>> history() async {
    final response = await http.get(
      Uri.parse('http://172.26.16.113:81/api/deliveriesHistory/${widget.sellId}'),
      headers: {
        HttpHeaders.authorizationHeader: 'Bearer ${g.userModel.token}',
         },
       );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      var jsonArray = jsonData['data'];
      List<HistoryDetail> productDetail = [];
      for (var jsonProducts in jsonArray) {
          print(response.body);
          HistoryDetail product = HistoryDetail(
          customerId: g.convertModelString(jsonProducts['customer_id']),
          customerAddress: g.convertModelString(jsonProducts['customer_address']),
          shopAddress: g.convertModelString(jsonProducts['shop_address']),
          productDetail: List<ProductDetail>.from(jsonProducts["product_detail"].map((x) => ProductDetail.fromJson(x))),
          paymentStatus: g.convertModelString(jsonProducts['payment_status']),
          totalPrice: g.convertModelString(jsonProducts['total_price']),
          shopPhone: g.convertModelString(jsonProducts['shop_phone']),
          createdAt: g.convertModelString(jsonProducts['created_at']),
          customerName: g.convertModelString(jsonProducts['customer_name']),
          shopName: g.convertModelString(jsonProducts['name']),
          sellOrderId: g.convertModelString(jsonProducts['sell_order_id']),
          customerPhone: g.convertModelString(jsonProducts['customer_phone']),
          status: jsonProducts['status'],
        );
        productDetail.add(product);
      }
      return productDetail;
    } else {
      throw Exception('Failed to load album');
    }
  }
  @override
  void initState() {
    super.initState();
    setCustomMapPintk();
    setCustomMapPinShop();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          brightness: Brightness.dark,
          elevation: 0.0,
          title:  Text("history_detail".translate().toFirstUppercaseWord()),
        ),
        body: FutureBuilder<List<HistoryDetail>>(
            future: history(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<HistoryDetail> history = snapshot.data!;
                print('======================================');
                print(history.length.toString() + 'hello');
                return Stack(
                    children: List.generate(history.length, (index) {
                  return Column(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  child: (history[index].status == 'Disable')
                                      ? Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 5, horizontal: 30),
                                          decoration: BoxDecoration(
                                            color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child:  Text(
                                            'failed'.translate().toFirstUppercase(),
                                            style: const TextStyle(color: Colors.white),
                                          ),
                                        )
                                      : Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 5, horizontal: 10),
                                          decoration: BoxDecoration(
                                            color:Colors.blueAccent,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child:  Text(
                                            'completed'.translate().toFirstUppercase(),
                                            style: const TextStyle(color: Colors.white),
                                          ),
                                )),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.home,
                                          color: Colors.grey,
                                          size: 23,
                                        ),
                                        Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 10),
                                          child: Text(
                                            history[index].shopName,
                                            style: const TextStyle(
                                                fontSize: 15,
                                                color: Colors.blue,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 37),
                                    child: Text(
                                      g.convertPhoneNumber(history[index].shopPhone),
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.grey[600],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 30),
                                    child: Row(
                                      children: [
                                        //Icon(Icons.location_on_outlined, color: Colors.grey[500],),
                                        Container(
                                          width: MediaQuery.of(context).size.width / 2,
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          child: Text(
                                            history[index].shopAddress,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.grey[600]),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(
                                      g.stringDateTimeToFormatDays(
                                          history[index].createdAt),
                                      style: const TextStyle(
                                          fontSize:12,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Text(
                                      g.stringDateTimeToFormatTimes(
                                          history[index].createdAt),
                                      style: const TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.person_rounded,
                                  color: Colors.grey,
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10),
                                  child: Text(
                                    history[index].customerName,
                                    style: const TextStyle(
                                        fontSize: 15,
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2,
                            padding: const EdgeInsets.symmetric(horizontal: 37),
                            child: Text(
                              g.convertPhoneNumber(history[index].customerPhone),
                              style: TextStyle(
                                  fontSize: 14, color: Colors.grey[600]),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 37),
                            child: Column(
                              children: List.generate(
                                  history[index].productDetail.length, (value) {
                                return Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      history[index].productDetail[value].productName,
                                      style: const TextStyle(
                                          color: Colors.red,
                                          fontSize:14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(' ( ' +
                                        history[index]
                                            .productDetail[value]
                                            .size),
                                    Text(', ' +
                                        history[index].productDetail[value].sugar),
                                    Container(
                                        child: (history[index].productDetail[value].cream == 1)
                                            ? const Text(',' + 'Cream')
                                            : const Text(', ' + 'No cream')),
                                    Text(
                                      ',X' +
                                          history[index]
                                              .productDetail[value].items,
                                      style: const TextStyle(color: Colors.red),
                                    ),
                                    const Text(')'),
                                  ],
                                );
                              }),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 2,
                            padding: const EdgeInsets.symmetric(horizontal: 37),
                            child: Text(
                              history[index].customerAddress,
                              style: TextStyle(
                                  fontSize: 15, color: Colors.grey[600]),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Container(
                                // width: MediaQuery.of(context).size.width/2,
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(''+
                                  history[index].sellOrderId.toString(),
                                  style: const TextStyle(
                                      fontSize: 15, color: Colors.blue,
                                      fontWeight: FontWeight.bold

                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              const Text(
                                'Total price:',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14),
                              ),
                              Container(
                                padding: const EdgeInsets.symmetric(horizontal: 10),
                                child: Text(
                                  history[index].totalPrice+'\$',
                                  style: const TextStyle(
                                      fontSize: 14,
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height:10,
                          )
                        ],
                      ),
                      Expanded(
                        child: GoogleMap(
                            markers: _markers,
                            mapType: MapType.normal,
                            onMapCreated: _onMapCreated,
                            initialCameraPosition: const CameraPosition(
                              target: _center,
                              zoom: 14.0,
                            )),
                      )
                    ],
                  );
                }));
              } else if (snapshot.hasError) {
                print(snapshot.error);
                return Text('${snapshot.error}');
              }
              return const Center(
                  child: CircularProgressIndicator(
                color: Colors.amber,
              ));
            }));
  }
}