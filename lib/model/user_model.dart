import 'package:delivery_app/utils/g.dart'as g;
class UserModel {
  late String token;
  late String name;
  late String image;
  late String email;
  late String address;
  late String phone;
  late String role;


  UserModel({

    required this.token,
    required this.name,
    required this.image,
    required this.email,
    required this.address,
    required this.phone,
    required this.role
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
        token: json['token'],
        address: g.convertModelString(json['address']),
        name: g.convertModelString(json['name']),
        phone: g.convertModelString(json['phone']),
        email: g.convertModelString(json['email']),
        image: g.convertModelString(json['image']) == null ? '' : g.convertModelString(g.convertModelString(json['image'])),
        role: g.convertModelString(json['user_role']),
    );
  }

}






