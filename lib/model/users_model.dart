class RegisterModel {
  late final String id;
  late final String name;
  late final String email;
  late final String emailVerifiedAt;
  late final String password;
  late final String phone;
  late final String address;
  late final String image;
  late final String userRole;
  late final String rememberToken;
  late final String createdAt;
  late final String updatedAt;

  RegisterModel(
      {
       required this.id,
       required this.name,
       required this.email,
       required this.emailVerifiedAt,
       required this.password,
       required this.phone,
       required this.address,
       required this.image,
       required this.userRole,
       required this.rememberToken,
       required this.createdAt,
       required this.updatedAt
      });

  RegisterModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    password = json['password'];
    phone = json['phone'];
    address = json['address'];
    image = json['image'];
    userRole = json['user_role'];
    rememberToken = json['remember_token'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }
}
