import 'package:delivery_app/utils/g.dart' as g;
class Policy {
  late String description;

  Policy({required this.description});

  Policy.fromJson(Map<String, dynamic> json) {
    description = g.convertModelString(json['description']);
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['description'] = description;
    return data;
  }
}
