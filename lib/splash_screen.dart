import 'dart:async';
import 'package:delivery_app/Screen/login/login.dart';
import 'package:delivery_app/tap.dart';
import 'package:delivery_app/utils/g.dart';
import 'package:delivery_app/utils/g.dart';
import 'package:delivery_app/utils/sr.dart';
import 'package:delivery_app/utils/userandsession.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivery_app/utils/g.dart' as g;

class SplashScreen extends StatefulWidget {
  late String userSessionVersion;
  late String userName;
  late String userAvatar;
  late String userType;
  late String token;
  late String userEmail;
  late String userPhone;
  SplashScreen({Key? key, }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  VUserAndSession vUserAndSession =  VUserAndSession();
  final Future<SharedPreferences> prefs = SharedPreferences.getInstance();
  checkedLogins() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userSessionVersion = prefs.getString(SR.versionUserSession);
    var token = prefs.getString(SR.token);
    if( token != null && userSessionVersion == g.versionUserSession){
      vUserAndSession.initializeWhenSessionTrue();
      Navigator.pushNamedAndRemoveUntil(context, '/Tap', (Route<dynamic> route) => false);
    } else{
      Navigator.pushNamedAndRemoveUntil(context, '/LoginScreen', (Route<dynamic> route) => false);
    }
  }
  checkedLogin() async{
    if( g.userModel.token != null ){
      if (kDebugMode) {
        print(g.userModel.token);
      }
    Navigator.push(context, MaterialPageRoute(builder: (context)=>Tap()));
    } else{
      Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
    }
  }
  @override
  void initState() {
    super.initState();
    Timer(
      const Duration(seconds: 3),
          () =>checkedLogins()
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Stack(
          children: [
            Positioned(
              top: 240,
                left:40,
                child: Image.asset('assets/image/cloud.png',
              width: 80,
              height: 60,
            )),
           ClipRRect(
              child: Image.asset('assets/image/food-delivery-platform-removebg-preview.png',
                height:double.infinity,
                width:double.maxFinite,
                alignment: Alignment.center,
              ),
            ),
            Positioned(
                top: 240,
                left:40,
                child: Image.asset('assets/image/cloud.png',
                  width: 80,
                  height: 60,
                )
            ),
            Positioned(
                bottom: 10,
                child: Image.asset('assets/image/image_02.png',color: Colors.white54,
                )
            ),
            const Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: SizedBox(
                    height: 10,
                    child: LinearProgressIndicator()
                )
            )
        ]
      ),
    );
  }
}