import 'package:delivery_app/model/delivery_model.dart';
import 'package:delivery_app/renderdata/renderdata.dart';

import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/utils/g.dart' as g;


class DeliveryRenderData extends RenderRowData{

  @override
  Future<String> getURL(oldVal)async{
    return 'http://103.101.80.88:81/api/deliveries';
  }
  @override
  Future<String> getWhere(oldVal) async {
    // TODO: implement getWhere
    return '';
  }

  @override
  Future<bool> getShowSomeThingWrong(oldVal) async {
    // TODO: implement getWhere
    return true;
  }

  @override
  prepareDataDropDownDataRows(rowList) async {
    Map row = {
      'list' : await dataToDropdownData(dataList: rowList['data']),
      'total' : rowList['total'],
    };
    return row;
  }
  dataToDropdownData({required List dataList}){
    if(dataList != null){
      List<Data> _list= [];
      dataList.forEach((v) {
        _list.add(Data.fromJson(v));
      });
      //print(_list);
      return _list;
    }
    //return null;
  }
}