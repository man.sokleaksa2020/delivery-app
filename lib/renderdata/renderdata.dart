import 'package:delivery_app/renderdata/corender.dart';
import 'package:flutter/cupertino.dart';
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/renderdata/coscroll.dart' as coScroll;

class RenderRowData{

  String url = '';
  String q = '';
  String where = '';
  bool showSomeThingWrong = false;
  bool autoGetData = true;
  CoDataRow dataRow =  CoDataRow();
  Map<String, dynamic> field = {};

  Future getData(setState, {bool refresh : false, required Map<String, dynamic> fields})async{
    String coUrl = await getURL(url);
    url = coUrl;
    String coQ = await getQ(q);
    coQ = coQ != '' ? '&q=${coQ}' : '';
    String coWhere = await getWhere(where);
    if(fields != null){
      field.addAll(fields);
    }
    Map<String, dynamic> coFields = await getFields(field);
    bool coShowSomeThingWrong = await getShowSomeThingWrong(showSomeThingWrong);
    //print('=========URL========${coUrl}');
    await dataRow.getDataRowSetState(
        setState,
        url: coUrl,
        refresh: refresh,
        where: '${coQ}${coWhere}',
        showSomeThingWrong: coShowSomeThingWrong,
        fields: coFields,
        prepareData:(val) async {
          return await prepareDataDropDownDataRows(val);
        }
    );
  }
  Future processGetData(setState)async{
    await getData(setState, refresh: true, fields: {}).then((value){
      return value;
    });
  }
  Future<String> getURL(oldVal)async{
    return oldVal;
  }
  Future<String> getQ(oldVal)async{
    return oldVal;
  }
  Future<String> getWhere(oldVal)async{
    return oldVal;
  }
  Future<Map<String, dynamic>> getFields (oldVal)async{
    return oldVal;
  }
  Future<bool> getShowSomeThingWrong(oldVal)async{
    return oldVal;
  }
  prepareDataDropDownDataRows(rowList)async{
    Map row = {
      'list' : await dataRowsToDropdownData(dataList: rowList['rows_branch']['data']),
      'total' : rowList['rows_branch']['total'],
    };
    return row;
  }
  dataRowsToDropdownData({required List dataList}){
    if(dataList != null){
      List _list= [];
      dataList.forEach((v) {
        _list.add({
          'id' : v['id'],
          'name' : v['title'],
        });
      });
      return _list;
    }
    return null;
  }
  /// =========================================================================
  /// STATE MANAGEMENT
  late String id;
  dynamic coSetState;
  late BuildContext coContext;

  late ScrollController controller;

  void refreshState(){
    if(coSetState != null){
      coSetState(() {
      });
    }
  }

  void initialize({required dynamic setState, required BuildContext context,required String id}){
    coSetState = setState;
    coContext = context;
    this.id = id;
    firstState();
  }

  coScrollListener() {
    gCo.scrollRenderDataController(controller, RenderRowData: this).then((value){
      getMoreData(refresh: false);
    });
  }

  void firstState(){
    controller = ScrollController();
    controller.addListener(coScrollListener);
    if(autoGetData){
      getMoreData();
    }
  }

  Future getMoreData({refresh: true}) async {
    if(refresh && dataRow.list != null){
      if(dataRow.list.length > 10){
        gCo.scrollToTop(controller);
      }
    }
    getData(coSetState, refresh: refresh, fields: {}).then((res){
      /// printDebugScope
      //gCo.printDebugScope(output: json.encode(coRenderRowData.dataRow.list), note: coRenderRowData.url, fields: fields);
    });
  }
  Future<void> onRefresh() {
    return getMoreData(refresh: true);
  }
}