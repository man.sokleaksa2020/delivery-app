import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoScroll extends StatelessWidget {
  Widget child;
  ValueChanged<String> onRefresh;
  CoScroll( { required this.child ,required this.onRefresh
  }) ;
  @override
  Widget build(BuildContext context) {

    return RefreshIndicator(
      onRefresh: () async {
        return onRefresh('Change');
      },
      child: CupertinoScrollbar(
        child: CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: <Widget>[
            SliverToBoxAdapter(
              child: child,
            ),
          ],
        ),
      ),
    );
  }
}
