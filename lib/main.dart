import 'package:delivery_app/Screen/home/homeScreen.dart';
import 'package:delivery_app/Screen/login/login.dart';
import 'package:delivery_app/Screen/profile/profile.dart';
import 'package:delivery_app/route_generate.dart';
import 'package:delivery_app/splash_screen.dart';
import 'package:delivery_app/tap.dart';
import 'package:delivery_app/utils/appmodel.dart';
import 'package:delivery_app/utils/sr.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/helper/stringExtension.dart';
import 'package:delivery_app/local/t.dart' as _t;
import 'package:delivery_app/utils/g_co.dart' as gCo;

Future coInit()async{
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    systemNavigationBarColor: Colors.black,// navigation bar color
    statusBarColor: Colors.transparent, // status bar color
  ));
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? language = prefs.getString(SR.language);
  if(language!= '' && language!= null){
    g.lang = language;
    _t.lang = language;
  }
  WidgetsFlutterBinding.ensureInitialized();
  gCo.initialize(
      coDebug: true,
      coMainColor: Colors.amber,
      coBaseUrl: 'http://103.101.80.88:81/api/',
      coDefaultImageAsset: 'assets/img/placeholder_image.jpg',
      coBaseUrlImg:  'http://103.101.80.88:81/',
      coApiWithTimestamp: true,
      coLanguageModel: [
        LanguageModel(
            key: 'en',
            name: 'english'.translate().toFirstUppercase(),
            assetImage: 'assets/img/en-en.png'
        ),
        LanguageModel(
            key: 'kh',
            name: 'khmer'.translate().toFirstUppercase(),
            assetImage: 'assets/img/kh-kh.png'
        ),
         LanguageModel(
            key: 'pm',
            name: 'Myanmar',
            assetImage: 'assets/img/kh-kh.png'
        ),
      ],
      coUserSessionVersion: '',
      coCurrencyDefaultSymbols: ''
  );
}
void main() async{
WidgetsFlutterBinding.ensureInitialized();
   await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: 'AIzaSyCt3KKlZ3c64aHT_qdPwU2SNmccxdEgmnM',
      appId: '1:931347441192:android:470ed7a2d50b36f481771d',
      messagingSenderId: '931347441192',
      projectId: 'tracking-1f750',
    ),
  );
coInit().then((_){
  runApp( MyApp());
});
}

class MyApp extends StatelessWidget {
  AppModel appModel = AppModel();
   MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ScopedModel<AppModel>(
        model: appModel,
        child:  ScopedModelDescendant<AppModel>(
            builder: (context, child, model) {
              return  MaterialApp(
                navigatorKey: gCo.coNavigatorAppKey,
                debugShowCheckedModeBanner: false,
                theme:  ThemeData(
                  primaryColor: gCo.mainColor,
                  fontFamily: model.lang == 'kh' ? 'Chenla' : null,
                ),
                initialRoute: '/SplashScreen',
                onGenerateRoute: RouteGenerator.generateRoute,
              );
            }
        )
    );
  }
}
