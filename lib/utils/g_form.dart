import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'g_widget.dart' as gWidget;
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/utils/fieldproperty.dart' as gForm;


typedef CoDateWidgetBuilder = Widget Function(
    BuildContext context, String val);

class CoDate extends StatefulWidget {
  String text;
  ValueChanged<DateTime> onChanged;
  DateTime initialDateTime;
  bool disable;
  CoDateWidgetBuilder customBuilder;
  String dateFormatDisplay;

  CoDate(
      {
       required this.text,
       required this.onChanged,
       this.disable: false,
       required this.initialDateTime,
       required this.customBuilder,
       this.dateFormatDisplay : 'd MMM, y',
      });

  @override
  _CoDateState createState() => _CoDateState();
}

class _CoDateState extends State<CoDate> {
  var now =  DateTime.now();
  DateTime dateTimes =  DateTime.now();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  Widget showDatePicker() {
    return CupertinoDatePicker(
      mode: CupertinoDatePickerMode.date,
      initialDateTime: dateTimes,
      onDateTimeChanged: (newDateTime) {
        dateTimes = newDateTime;
      },
    );
  }

  void popUp(){
    if(widget.disable == false) {
      dateTimes = DateTime(widget.initialDateTime.year, widget.initialDateTime.month, widget.initialDateTime.day);
      gWidget.coModalBottomSheet(context, text: widget.text, scrollAble: false ,withOK: true, build: showDatePicker(),
          onOK: (){
            widget.initialDateTime =DateTime(dateTimes.year, dateTimes.month, dateTimes.day);
            setState(() {
            });
            Navigator.of(context, rootNavigator: true).pop();
            if(widget.onChanged != null){
              widget.onChanged(dateTimes);
            }
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    if(widget.initialDateTime == null){
      widget.initialDateTime =DateTime(dateTimes.year, dateTimes.month, dateTimes.day);
    } else {
      dateTimes =DateTime(widget.initialDateTime.year, widget.initialDateTime.month, widget.initialDateTime.day);
    }
    return widget.customBuilder == null ?const TextField(

    ) :
    InkWell(
        onTap: (){
          popUp();
        },
        child: widget.customBuilder(context, '${gCo.formatDateOfDay(widget.initialDateTime ?? dateTimes, format: widget.dateFormatDisplay)}')
    );
  }
}
