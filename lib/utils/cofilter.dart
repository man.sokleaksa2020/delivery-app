import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoFilter extends StatefulWidget {
 late String title;
  late Color color;
  late IconData icon;
  late GestureTapCallback onTap;
 late  String value;
  CoFilter({
    required this.title, required this.color,required this.onTap, required this.icon,}) ;
  @override
  _CoFilterState createState() => _CoFilterState();
}

class _CoFilterState extends State<CoFilter> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      child: Container(
        padding:const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.black45
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(widget.title, style:const TextStyle(fontSize: 14 ,color: Colors.white, fontWeight: FontWeight.bold),),
           const SizedBox(width: 5,),
            widget.value != null ? Container(
                margin:const EdgeInsets.only(right: 5),
                child: Text(': ${widget.value}', style:const TextStyle(fontSize: 14 ,color: Colors.white),)
            ) :const SizedBox(width: 0, height: 0,),
            Icon(widget.icon ?? Icons.arrow_drop_down, size: 16, color: Colors.white)
          ],
        ),
      ),
    );
  }
}
