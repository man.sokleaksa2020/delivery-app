import 'package:delivery_app/model/user_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:convert';
import 'package:delivery_app/utils/sr.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'vnotification.dart';

class AppModel extends Model {
  VNotification vNotification =VNotification();
  List<Map<String,String>> _outputDebug = [];
  List<dynamic> get outputDebug => _outputDebug;

  void printDebug(String output, {String note : '', required Map<String, dynamic> fields}){
    if(debug){
      _outputDebug.insert(0,{'note' : '$note', 'output' : '$output', 'fields' : json.encode(fields)});
      notifyListeners();
    }
  }
  String _lang = gCo.lang;
  String get lang => _lang;
  void clearDebug(){
    _outputDebug.clear();
    notifyListeners();
  }
  bool _debugShow = false;
  bool get debugShow => _debugShow;

  bool _debug = false;
  bool get debug => _debug;

  void debugAction(bool val){
    _debug = val;
    notifyListeners();
  }
  void languageAction(String val) {
    _lang = val;
    notifyListeners();
  }
  void debugShowAction(){
    _debugShow = !_debugShow;
    notifyListeners();
  }
  bool _keyboardShow = false;
  bool get keyboardShow => _keyboardShow;
  void keyboardAction(bool val){
    _keyboardShow = val;
    notifyListeners();
  }

  String _keyboardVal = '';
  String get keyboardVal => _keyboardVal;

  void keyboardValAction(String val){
    _keyboardVal = val;
    notifyListeners();
  }

  final UserModel _userModel = UserModel(token: '', name: '', image: '', email: '', address: '', role: '', phone: '');
  UserModel get userModel => _userModel;
  void userActions({required String token,required String userName, required String userPhone,required String userAvatar,required String userEmail,required String userType,required String language })async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(token != '' && token != null){
      prefs.setString(SR.token, token.toString());
      g.userModel.token = token.toString();
      _userModel.token = token.toString();
    }

    if(userName != '' && userName != null){
      prefs.setString(SR.userName, userName.toString());
      g.userModel.name = userName.toString();
      _userModel.name = userName;
    }
    if(userPhone != '' && userPhone != null){
      prefs.setString(SR.userPhone, userPhone.toString());
      g.userModel.phone = userPhone.toString();
      _userModel.phone = userPhone;
    }
    if(userEmail != '' && userEmail != null){
      prefs.setString(SR.userEmail, userEmail.toString());
      g.userModel.email = userEmail.toString();
      _userModel.email = userEmail;
    }
    if(userType != '' && userType != null){
      prefs.setString(SR.userType, userType.toString());
      g.userModel.role = userType.toString();
      _userModel.role = userType;
    }
    if(userAvatar != '' && userAvatar != null){
      prefs.setString(SR.userAvatar, userAvatar.toString());
      g.userModel.image = userAvatar.toString();
      _userModel.image = userAvatar;
    }

    notifyListeners();
  }

  void userAction({required String token,required String userName, required String userPhone,required String userAvatar,required String userEmail,required String userType,required String language,})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(token != '' && token != null){
      prefs.setString(SR.token, token.toString());
      g.userModel.token = token.toString();
      _userModel.token = token.toString();
    }

    if(userName != '' && userName != null){
      prefs.setString(SR.userName, userName.toString());
      g.userModel.name = userName.toString();
      _userModel.name = userName;
    }
    if(userPhone != '' && userPhone != null){
      prefs.setString(SR.userPhone, userPhone.toString());
      g.userModel.phone = userPhone.toString();
      _userModel.phone = userPhone;
    }
    if(userEmail != '' && userEmail != null){
      prefs.setString(SR.userEmail, userEmail.toString());
      g.userModel.email = userEmail.toString();
      _userModel.email = userEmail;
    }
    if(userType != '' && userType != null){
      prefs.setString(SR.userType, userType.toString());
      g.userModel.role = userType.toString();
      _userModel.role = userType;
    }
    if(userAvatar != '' && userAvatar != null){
      prefs.setString(SR.userAvatar, userAvatar.toString());
      g.userModel.image = userAvatar.toString();
      _userModel.image = userAvatar;
    }

    notifyListeners();
  }
  Future userActionss({ String? token, String? userId, String? userName, String? userPhone,
  String? userAvatar, String? userEmail, String? language, String? versionAppLanguage})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    if(token != '' && token != null){
      prefs.setString(SR.token, token.toString());
      g.userModel.token = token.toString();
    _userModel.token = token.toString();
    }


    if(versionAppLanguage != '' && versionAppLanguage != null){
      prefs.setString(SR.versionAppLanguage, versionAppLanguage.toString());
    }
    if(language != '' && language != null){
      gCo.lang = language.toString();
      AppModel appModel = g.getAppModel();
      appModel.languageAction(gCo.lang);
      prefs.setString(SR.language, language.toString());
      print('language.toString()=================================================');
      print(language.toString());
    }
    if(userName != '' && userName != null){
      prefs.setString(SR.userName, userName.toString());
      g.userModel.name = userName.toString();
      _userModel.name = userName.toString();
    }
    if(userPhone != '' && userPhone != null){
      prefs.setString(SR.userPhone, userPhone.toString());
      g.userModel.phone = userPhone.toString();
      _userModel.phone = userPhone.toString();
    }
    if(userEmail != '' && userEmail != null){
      prefs.setString(SR.userEmail, userEmail.toString());
      g.userModel.email = userEmail.toString();
      _userModel.email = userEmail.toString();
    }
    if(userAvatar != '' && userAvatar != null){
      prefs.setString(SR.userAvatar, userAvatar.toString());
      //g.userAvatar = userAvatar.toString();
      g.userModel.image = userAvatar.toString();
      _userModel.image = userAvatar.toString();
    }
    if(gCo.debug){
    }
    notifyListeners();
  }
}