import 'dart:convert';
import 'dart:async';
import 'package:delivery_app/Screen/login/login.dart';
import 'package:delivery_app/model/shop_model.dart';
import 'package:delivery_app/model/upload_model.dart';
import 'package:delivery_app/utils/appmodel.dart';
import 'package:delivery_app/utils/sr.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/model/user_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivery_app/local/t.dart' as _t;

import 'g_co.dart';
String lang = _t.lang;
String versionApp = '1.0';
String versionUserSession = '1.2';
double versionDownloadNew = 1.0;
double versionDownloadMustNew = 1.0;
UserModel userModel = UserModel( email: '', address: '',  name: '', image: '', phone: '', token: '', role: '');
Upload uploadImage = Upload(fileName: '', pathFile: '', type: '', name: '', extension: '', path: '');
late String ProductId;

GlobalKey<ScaffoldState> scaffoldTapKey = GlobalKey<ScaffoldState>();

String formatSqlWithTime = 'yyyy-MM-dd HH:mm:ss';

const durationGetVersionAsset =  Duration(seconds: 10);

const durationTracking = Duration(seconds: 10);

String userSessionVersion = '2.2';

Future<Map<String, dynamic>?> getTokenMap() async {
  Map tokenMap = {};
  var userModel;
  tokenMap['token'] = userModel.token;
}
List<UserModel> parseProducts(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<UserModel>((json) => UserModel.fromJson(json)).toList();
}
int randomDouble(){
  DateTime myTime = DateTime.now();
  return int.parse('${myTime.year}${myTime.month}${myTime.day}${myTime.hour}${myTime.minute}${myTime.second}${myTime.millisecond}');
}
double dateTimeToDouble(DateTime myTime) => double.parse('${myTime.year}${myTime.month}${myTime.day}${myTime.hour}${myTime.minute}${myTime.second}${myTime.millisecond}');


stringDateTimeToFormatDates(String dateTime, {String format : 'y-MM-dd'}){
  DateTime dt =  DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  return gCo.formatDateOfDay(dt, format: format);
}
stringDateTimeToFormatDays(String dateTime, {String format : 'd MMMM, y'}){
  DateTime dt =  DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  return gCo.formatDateOfDay(dt, format: format);
}
stringDateTimeToFormatDay(String dateTime, {String format : 'd MMMM, y'}){
  DateTime dt = DateFormat("yyyy-M-dd").parse(dateTime);
  return gCo.formatDateOfDay(dt, format: format);
}

stringDateTimeToFormatTimes(String dateTime){
  DateTime dt = DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  final timeFormat = DateFormat.jm(); //"6:00 AM"
  String time = timeFormat.format(dt);
  return time;
}
stringDateTimeToFormatTime(String dateTime){
  DateTime dt = DateFormat("HH:mm:ss").parse(dateTime);
  final timeFormat = DateFormat.jm(); //"6:00 AM"
  String time = timeFormat.format(dt);
  return time;
}
String dateTimeToFormatTimeWithSecond(DateTime now){
  var outputFormat = DateFormat('hh:mm:ss a');
  return outputFormat.format(now);
}
Future<Position> coCheckLocationPermission() async {
  bool serviceEnabled;
  LocationPermission permission;

  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    return Future.error('Location services are disabled.');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.deniedForever) {
    return Future.error(
        'Location permissions are permantly denied, we cannot request permissions.');
  }

  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission != LocationPermission.whileInUse &&
        permission != LocationPermission.always) {
      return Future.error(
          'Location permissions are denied (actual value: $permission).');
    }
  }
  return await Geolocator.getCurrentPosition();
}

Future<Marker> coAddMarker({required LatLng coLatLng, required String coID, required String coTitle, required String coDescription, required BitmapDescriptor coPinLocationIcon})async {
  Marker markers = Marker(

    markerId: MarkerId(coID.toString()),
    position: coLatLng,
    infoWindow: coTitle == null ? InfoWindow.noText : InfoWindow(
      title: coTitle,
      snippet: coDescription,
    ),
    icon: coPinLocationIcon ?? BitmapDescriptor.defaultMarker,
  );
  return markers;
}
AppModel getAppModel(){
  final BuildContext context = gCo.coContext;
  AppModel model = ScopedModel.of(context);
  return model;
}
void logout(context)async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.remove(SR.token);
  prefs.remove(SR.userId);
  prefs.remove(SR.userName);
  prefs.remove(SR.userPhone);
  prefs.remove(SR.userType);
  prefs.remove(SR.userAvatar);
  prefs.remove(SR.userEmail);
  prefs.remove(SR.agencyId);
  Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
}

void refreshUserInfo({required String token}){
  String url = 'http://103.101.80.88:81/api/users';
  var userModel;
  Map<String, dynamic> fields = {
    'token' : userModel.token,

  };
  gCo.get(url: url, fields: fields).then((respond){
    var res = json.decode(respond);
    if (SR.toInt(res['id'].toString())! > 0) {
      getAppModel().userAction(
        userEmail: res['email'].toString(),
        userName: res['username'].toString(),
        userPhone: res['phone'].toString(),
        userAvatar: res['image'].toString(),
        token: res['token'].toString(),
        userType:res['user_role'],
        language: '',

      );
    }
  });
}
 stringDateTimeToFormatDate(String dateTime){
  DateTime dt = DateFormat("dd-MM-yyyy").parse(dateTime);
  return gCo.formatDateOfDay(dt, format: 'd MMMM,y ');
}
stringDateTimeToFormatDateReal(String dateTime, {String format : 'd MMM, y'}){
  DateTime dt = DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  return gCo.formatDateOfDay(dt, format: format);
}
DateTime stringDateTimeToFormatDateObject(String dateTime){
  DateTime dt = DateFormat("dd-MM-yyyy").parse(dateTime);
  return gCo.formatDateOfDay(dt, format: 'd MMMM, y', returnObject: true);
}

DateTime? stringDateTimeToFormatDateObjectReal(String dateTime){
  try{
    return DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  }catch(_) {
    return null;
  }
}
TimeOfDay stringTimeToFormatTimeOfDayObject(String time){
   return TimeOfDay(hour:int.parse(time.split(":")[0]),minute: int.parse(time.split(":")[1]));
}

String parseHtmlString(String htmlString) {
  var document = parse(htmlString);
  String parsedString = parse(document.body.text).documentElement.text;
  return parsedString;
}
String convertModelString(dynamic text, ){
  text = (text == null ? '' : text.toString());
  String coText = text == null || text == 'null' ? '' : text;
  return coText;
}
num? convertModelNumber(dynamic stringNumber){
  num? coNumber = num.tryParse(stringNumber != null && stringNumber != '' && stringNumber != 'null' ? stringNumber : '0');
  return coNumber;
}
String convertPhoneNumber(String phoneNumber ){
  String formattedPhoneNumber = phoneNumber.replaceAllMapped(RegExp(r'(\d{3})(\d{3})(\d+)'), (Match m) => "${m[1]} ${m[2]} ${m[3]}");
  return formattedPhoneNumber;
}
String convertPhoneNumbers(String phoneNumber ){
  String formattedPhoneNumber = phoneNumber.replaceAllMapped(RegExp(r'(\d{3})(\d{3})(\d{3})(\d+)'), (Match m) => "${m[1]} ${m[2]} ${m[3]} ${m[4]}");
  return formattedPhoneNumber;
}