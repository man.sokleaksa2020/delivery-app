import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class SR {
  static const securityLoginId = 'securityLoginId';
  static const token = 'token';
  static const userId = 'userId';
  static const agencyId = 'agencyId';
  static const privateKey = 'privateKey';
  static const userFirebaseId = 'userFirebaseId';
  static const userType = 'userType';
  static const language = 'language';
  static const userEmail = 'userEmail';
  static const password = 'password';
  static const userName = 'userName';
  static const userPhone ='userPhone';
  static const userAvatar ='userAvatar';
  static const versionUserSession ='userSessionVersion';
  static const versionAppLanguage ='versionAppLanguage';
  static const inOrOutStatus ='inOrOutStatus';
  static const qrCodeToken ='qrCodeToken';
  static const showAvailableUpdate ='showAvailableUpdate';


  static double? toDouble(dynamic n) {
    var r = n ?? 0.0;
    try {
      return double.tryParse(r.toString());
    } catch (e) {
      return 0.0;
    }
  }

  static int? toInt(dynamic n) {
    var r = n ?? 0;
    try {
      return int.tryParse(r.toString());
    } catch (e) {
      return 0;
    }
  }

  static String getRandomString(int length) {
    var rand =  Random();
    var codeUnits = List.generate(length, (index) {
      return rand.nextInt(33) + 89;
    });

    return String.fromCharCodes(codeUnits);
  }

  static Widget myImage(url, [width = 0.0, height = 0.0, l = 0]) {
    if (width > 0 && height > 0) {
      return CachedNetworkImage(
//        errorWidget:
//            l > 10 ? new Icon(Icons.error) : myImage(url, width, height, l + 1),
        // errorWidget: (context, url, error) => new Icon(Icons.error),
        errorWidget: (context, url, error) =>
            Image.asset("assets/placeholder/placeholder_image.png"),
        fit: BoxFit.cover,
        width: width,
        height: height,
        imageUrl: url,
        //placeholder: (context, url) => new CircularProgressIndicator(),
        placeholder: (context, url) => Image.asset(
            'assets/placeholder/placeholder_image.png',
            width: width,
            height: height),
      );
    } else {
      return CachedNetworkImage(
//        errorWidget:
//            l > 10 ? new Icon(Icons.error) : myImage(url, width, height, l + 1),
        // errorWidget: (context, url, error) => new Icon(Icons.error),
        errorWidget: (context, url, error) =>
            Image.asset("assets/placeholder/placeholder_image.png"),
        fit: BoxFit.cover,
        imageUrl: url,
        // placeholder: (context, url) => new CircularProgressIndicator(),
        placeholder: (context, url) => Image.asset(
            'assets/placeholder/placeholder_image.png',
            width: width,
            height: height),
      );
    }
  }

  static Widget myImageC(url, [width = 0.0, height = 0.0, l = 0]) {
    if (width > 0 && height > 0) {
      return CachedNetworkImage(
        // errorWidget: (context, url, error) => new Icon(Icons.error),
        errorWidget: (context, url, error) =>
            Image.asset("assets/placeholder/placeholder_image.png"),
        fit: BoxFit.contain,
        width: width,
        height: height,
        imageUrl: url,
        // placeholder: (context, url) => new CircularProgressIndicator(),
        placeholder: (context, url) => Image.asset(
            'assets/placeholder/placeholder_image.png',
            width: width,
            height: height),
      );
    } else {
      return CachedNetworkImage(
        // errorWidget: (context, url, error) => new Icon(Icons.error),
        errorWidget: (context, url, error) =>
            Image.asset("assets/placeholder/placeholder_image.png"),
        fit: BoxFit.contain,
        imageUrl: url,

        placeholder: (context, url) =>  Image.asset(
            'assets/placeholder/placeholder_image.png',
            width: width,
            height: height),
      );
    }
  }

  static Widget _buildProgressIndicator(bool isPerformingRequest) {
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child:  Center(
        child:  Opacity(
          opacity: isPerformingRequest ? 1.0 : 0.0,
          child:  CircularProgressIndicator(),
        ),
      ),
    );
  }

  static String capitalize(String str) {
    str = str.toLowerCase();
    List<String> words = str.split(" ");
    String capitalizeWord = "";
    for (String w in words) {
      String first = w.substring(0, 1);
      String afterfirst = w.substring(1);
      capitalizeWord += ((first.toUpperCase() + afterfirst) + " ");
    }
    return capitalizeWord.trim();
  }
  static String setIsN(String str) {
    //return str;
    if (str == null || str == 'null' || str == '') {
      return '';
    }
    return str;
  }
}
