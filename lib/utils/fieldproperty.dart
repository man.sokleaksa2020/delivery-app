import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'g.dart' as gCo;

class FieldProperty {
  static const colors = Colors.black87;
  final fieldcolor = colors;
  static const colorFocus = Colors.blue;
  final fieldContentPadding = const EdgeInsets.only(left: 17.0, top: 39.0, bottom: 0);
  final fieldContentPaddingSmall = const EdgeInsets.fromLTRB(10.0, 23.0, 10.0, 10.0);
  final fieldStyle = const TextStyle(
      fontWeight: FontWeight.normal, fontSize: 15, color: Colors.black87);
  final fieldBorder = OutlineInputBorder(
    borderSide:
    const BorderSide(color: Colors.blue, width: 1.5),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldBorderError = OutlineInputBorder(
    borderSide:
    const BorderSide(color: Colors.redAccent, width: 1.5),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldEnabledBorder = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.black38, width: 1),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldEnabledBorderError = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.redAccent, width: 1),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final fieldEnabledBorderWithValue = OutlineInputBorder(
    borderSide: const BorderSide(color: Colors.black38, width: 1),
    borderRadius: BorderRadius.circular(6),
    gapPadding: 10,
  );
  final TextStyle fieldLabelStyle = const TextStyle(
    color: Colors.black54,
    fontSize: 15,
  );
  final TextStyle fieldLabelHasFocusStyle = const TextStyle(
    color: Colors.blue,
    fontSize: 19,
  );
}

enum coFormSize {small, normal, medium}
Map noSelectItemRow = {
  'id' : 'null',
  'name' : 'please_select_item'
};
Future<List> addNoSelect(List list)async{
  List cList = [];
  cList.add(noSelectItemRow);
  cList.addAll(list);
  return cList;
}

void onLoading(context, {text}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        backgroundColor: Colors.transparent,
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(
                Radius.circular(10.0),
              )),
          padding: const EdgeInsets.symmetric(vertical: 22.0, horizontal: 5.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("${text ?? 'please_wait'}"),
              const SizedBox(
                height: 20.0,
              ),
              const CircularProgressIndicator(),
            ],
          ),
        ),
      );
    },
  );
}
