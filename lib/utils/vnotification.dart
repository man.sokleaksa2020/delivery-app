import 'dart:convert';
import 'dart:io';
import 'package:delivery_app/utils/appmodel.dart';
import 'package:delivery_app/utils/g_co.dart' as gCo;
import 'package:delivery_app/utils/sr.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:flutter/cupertino.dart';
import 'package:scoped_model/scoped_model.dart';


class VScopeVariable {
  late String id;
  dynamic coSetState;
  late BuildContext coContext;

  void refreshState(){
    if(coSetState != null){
      coSetState(() {
      });
    }
  }

  void initialize({required dynamic setState, required BuildContext context,required String id}){
    coSetState = setState;
    coContext = context;
    this.id = id;
    firstState();
  }

  void firstState(){

  }
}
class VNotification extends VScopeVariable {
  final String fileName = 'notification4.txt';

  bool onGetting = false;
  Future initializeWhenSessionTrue()async{
    AppModel appModel = ScopedModel.of(gCo.coContext);
 //   appModel.notificationListAction(getFromLocal: true);
  }
}
