import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'package:delivery_app/model/user_model.dart';
import 'package:delivery_app/utils/appmodel.dart';
import 'package:delivery_app/utils/vnotification.dart';
import 'package:flutter/cupertino.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:delivery_app/utils/sr.dart';
import 'package:delivery_app/utils/g.dart' as g;
import 'package:delivery_app/utils/g_co.dart' as gCo;

import 'g.dart';


class VUserAndSession extends VScopeVariable{


  Future initializeWhenSessionTrue()async{
    AppModel appModel = ScopedModel.of(gCo.coContext);


    /// Tracking

    return null;
  }



  void clearAsset()async{
    AppModel appModel = g.getAppModel();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(SR.token);
    prefs.remove(SR.userId);
    prefs.remove(SR.userName);
    prefs.remove(SR.userPhone);
    prefs.remove(SR.userType);
    prefs.remove(SR.userAvatar);
    prefs.remove(SR.userEmail);
    prefs.remove(SR.agencyId);
  }
}