import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:delivery_app/widget/scroll.dart';
import 'package:shimmer/shimmer.dart';
import 'g_co.dart' as gCo;
import 'package:delivery_app/utils/g_widget.dart' as gWidget;


enum coButtonSize {big, medium, small}

void showSMSFailValidation() {
  coShowMessageDialog(
      title: 'validation_failed',
      message: 'please_input_required_fields',
      messageDialogType: MessageDialogType.fail);
}
void showSMSSomeThingWrong({required String title,required String message}) {
  coShowMessageDialog(
      title: title == null ? 'something_wrong': title,
      message: message == null ? 'data_response_error': message,
      messageDialogType: MessageDialogType.fail);
}

Widget someThingWrongWidget(context, {required String title}){
  return Container(
    width: MediaQuery.of(context).size.width,
    child: Center(
      child: Container(
        height: 200,
        child: Column(
          children: [
           const Icon(Icons.wifi_off, size: 30,),
            const SizedBox(height: 10,),
            Text(title == null ? 'no_internet_connection': title, style: TextStyle(fontSize: 19, fontWeight: FontWeight.w400),),
          ],
        ),
      ),
    ),
  );
}

Widget debugWidget(context,{required String output}){
  if(gCo.debug == false){
    return coZero();
  }
  return Column(
    children: <Widget>[
      Container(
        padding:const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        //width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.purple, width: 1)
        ),
        margin:const EdgeInsets.only(top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
          const  Center(child: Text('Output')),
           const SizedBox(height: 10,),
            if (output != '') Container(
              //width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              color: Colors.indigo,
              child: Text(
                gCo.parseHtmlString(gCo.getPrettyJSONString(output.toString())),
                style:const TextStyle(color: Colors.cyanAccent),
              ),
            ) else coZero(),
          ],
        ),
      ),
    ],
  );
}

/*void imageViewer(context, {required String url}){
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => ImageViewer()),
  );
}*/

void onLoading(context, {text}) {
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        backgroundColor: Colors.transparent,
        child: Container(
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius:  BorderRadius.all(
                Radius.circular(10.0),
              )),
          padding:const EdgeInsets.symmetric(vertical: 22.0, horizontal: 5.0),
          child:  Column(
            mainAxisSize: MainAxisSize.min,
            children: [
            Text("${text ?? 'please wait'}", style:const TextStyle(
              color:Colors.black
            )),
             const SizedBox(
                height: 20.0,
              ),
               const CircularProgressIndicator(
                 color: Colors.amber,
               ),
            ],
          ),
        ),
      );
    },
  );
}
Widget debugFooterWidget(context, {required List<Map<String,String>> output}){
  if(gCo.debug == false){
    return coZero();
  }
 var controller;
 return Container(
    padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
    width: MediaQuery.of(context).size.width,
 decoration: BoxDecoration(
      border: Border.all(color: Colors.white, width: 1)
    ),
      child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
     const  Center(child: Text('Debug Output', style: TextStyle(fontSize: 13),)),
     const   SizedBox(height: 8,),
        Expanded(
          child: CoScroll(
            child: Column(
              children: List.generate(output != null ? output.length : 0, (index){
                return  Container(
                  padding: const EdgeInsets.only(bottom: 15),
                  margin: const EdgeInsets.only(bottom: 20),
                  width: MediaQuery.of(context).size.width,
                  decoration:const BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.cyanAccent))
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      gCo.checkKeyMap(output[index], 'note') ? (output[index]['note'] == '' || output[index]['note'] == 'null' || output[index]['note'] == null ? coZero() :
                      Container(
                        margin: const EdgeInsets.only(bottom: 10),
                        child: Text(
                          'NOTE: ${output[index]['note']}',
                          style: const TextStyle(color: Colors.cyanAccent),
                        ),
                      )) : coZero(),
                      gCo.checkKeyMap(output[index], 'fields') ? (
                          output[index]['fields'] != null && output[index]['fields'] != 'null' && output[index]['fields'] != '' ? (
                              output[index]['fields']!.isNotEmpty ?
                              Text(
                              '')
                                  : coZero()) : coZero()
                      ) : coZero(),
                      Text(''
                       // gCo.checkKeyMap(output[index], 'output') ? gCo.parseHtmlString(gCo.getPrettyJSONString(output[index]['output'])) : '',
                       // style: TextStyle(color: Colors.cyanAccent),
                      ),
                    ],
                  ),
                );
              }),
            ),
       // scrollController:controller,
            onRefresh: (String value) {  },
          ),


    ),
  ]
  )
  );
}


newDialog(BuildContext context, {String title : 'Title', required Widget build, required Color headColor, height : 450.0}){
  headColor = (headColor ?? Theme.of(context).primaryColor);
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return Dialog(
        backgroundColor: Colors.transparent,
        child: Container(
          height: height,
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  color: headColor,
                  borderRadius: const BorderRadius.only(
                      topLeft:  Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                ),
                padding:const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                child: Center(
                  child: Text(
                    title,
                    style: const TextStyle(color: Colors.white,fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                ),
              ),
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight:  Radius.circular(10.0)),
                  ),
                  child: build ?? coZero()
              ),
            ],
          ),
        ),
      ) ;
    },
  );
}

coShowConfirm({context, title,  Widget? buildContent ,String? content, void Function()? coYesFunction,  void Function()? coNoFunction, yesIsDestructiveAction: true,}){
  return  showDialog(
    context: context,
    builder: (context) => CupertinoAlertDialog(
      title:  Text('${title ?? 'Are you sure'}?',textAlign: TextAlign.center, style: const TextStyle(color: Colors.black)),
      content: buildContent ?? Text(content ?? 'you_want_to_delete_this_item_from_your_cart',textAlign: TextAlign.center, style: const TextStyle(color: Colors.black)),
      actions: <Widget>[
        CupertinoDialogAction(
          isDestructiveAction: yesIsDestructiveAction,
          onPressed: () async {
            Navigator.of(context).pop(true);
            return coYesFunction!();
          },
          child: const Text('yes'),
        ),
        CupertinoDialogAction(
          onPressed: () {
            Navigator.of(context).pop(false);

          },
          child: const Text('no'),
        ),
      ],
    ),
  );
}

enum MessageDialogType{fail, warning, successful}

Widget coZero(){
  return const SizedBox(width: 0, height: 0,);
}

void coShowMessageDialog({required String title, message, MessageDialogType messageDialogType : MessageDialogType.successful, int durationInSeconds : 3}){
 final context = gCo.coContext;

  Color color = Colors.green;
  IconData icon = Icons.check_circle_outline;
  if(messageDialogType == MessageDialogType.fail){
    color = Colors.redAccent;
    icon = Icons.close;
  } else if(messageDialogType == MessageDialogType.warning){
    color = Colors.orange;
    icon = Icons.info_outline;
  }

  Flushbar(
    flushbarPosition: FlushbarPosition.TOP,
    //flushbarStyle: FlushbarStyle.GROUNDED,
    titleText: Text(title = 'undo_successful'),
    messageText: Text(message ?? 'item_is_already_undo'),
    icon: Icon(
      icon,
      size: 28.0,
      color: color,
    ),
    duration: Duration(seconds: durationInSeconds),
    animationDuration:const Duration(milliseconds: 500),
    //leftBarIndicatorColor: Colors.green[300],
    margin: const EdgeInsets.all(8),
    borderRadius: 8,
    backgroundColor: Colors.white,
    borderColor: color,
    boxShadows: [
      BoxShadow(color: Colors.blue[400]!,
        offset:const Offset(0.0, 2.0),
        blurRadius: 3.0,)
    ],
  )
    .show(context);
}

Widget loadingWidget(context, {coLength = 5}){
  return Column(
      children: List.generate( coLength, (index) {
        return  Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Theme.of(context).hintColor.withOpacity(0.15),
                  offset: const Offset(0, 3),
                  blurRadius: 10)
            ],
          ),
          margin:const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
          child: Shimmer.fromColors(
            period:const Duration(milliseconds: 700),
            baseColor: Colors.grey[100]!,
            highlightColor: Colors.grey,
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                          children: <Widget>[
                            Container(color: Colors.black, height: 10,),
                           const SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            const SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                           const  SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            const SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            const SizedBox(height: 10,),
                          ]
                      ),
                    ),
                   const SizedBox(width: 20,),
                    Expanded(
                      child: Column(
                          children: <Widget>[
                            Container(color: Colors.black, height: 10,),
                           const SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                           const SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                            const SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                           const SizedBox(height: 10,),
                            Container(color: Colors.black, height: 10,),
                           const SizedBox(height: 10,),
                          ]
                      ),
                    ),
                  ],
                )
            )
            ,
          ),
        );
      })
  );
}

void coAction(context,{required String title, List<Widget>? actions} ){
  /*CupertinoActionSheetAction(
    child: Text("Disable"),
    isDestructiveAction: true,
    onPressed: () {
      Navigator.pop(context);

      });*/
  final action = CupertinoActionSheet(
    title: Text(
      title == null ? 'action' : title,
      style: const TextStyle(fontSize: 20),
    ),
    message: Column(
      children: const <Widget>[
       Text(
          "click_any_action",
          style: TextStyle(fontSize: 15.0),
        ),
      ],
    ),
    actions: actions,
    /// close
    cancelButton: CupertinoActionSheetAction(
      child: const Text("close"),
      onPressed: () {
        Navigator.pop(context);
      },
    ),
  );
  showCupertinoModalPopup(
      context: context, builder: (context) => action);
}

Widget imageUrlWidget(imageUrl, { BoxFit fit : BoxFit.cover, required String errorImageAsset}){
  if(errorImageAsset == null){
    errorImageAsset = gCo.defaultImageAsset;
  }
  if(imageUrl == null || imageUrl == 'null' || imageUrl == '' ||  imageUrl == '${gCo.baseUrlImg}null' ||  imageUrl == gCo.baseUrlImg){
    return Image.asset(
      errorImageAsset,
      fit: fit,
    );
  }
  return CachedNetworkImage(
    placeholder: (context, url) {
      return Shimmer.fromColors(
        period: Duration(milliseconds: 700),
        baseColor: Colors.grey,
        highlightColor: Colors.grey,
        child: Container(
          color: Colors.white,
        ),
      );
    },
    imageUrl: '${imageUrl}',
    fit: fit,
    errorWidget: (context, url, error) => Image.asset(
      errorImageAsset,
      fit: fit,
    ),
  );
}
void coModalBottomSheet(context, {String text : 'Bottom Sheet', required Widget build, bool withOK : false, bool fullScreen : false, required GestureTapCallback onOK, bool scrollAble : true, Color backgroundColor: Colors.white}) async {
  showCupertinoModalPopup(
    //useRootNavigator: false,
    //semanticsDismissible: true,
    //isScrollControlled: true,
    //isDismissible: true,
      context: context,
      builder: (BuildContext bc) {
        return Container(
          width: MediaQuery.of(context).size.width,
          decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(10.0),
                  topRight: const Radius.circular(10.0))),
          height: fullScreen ? MediaQuery.of(context).size.height - 56 : MediaQuery.of(context).size.height / 2,
          child: Material(
            color: Colors.transparent,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: IconButton(
                          icon: Icon(Icons.close),
                          onPressed: () {
                            Navigator.of(context, rootNavigator: true).pop();
                          }),
                    ),
                    Expanded(
                      flex: 5,
                      child: Center(
                        child: Text(
                          '${text}',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.black87,
                            fontWeight: FontWeight.w500,
                            fontSize: 18.0,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: withOK == true ? InkWell(
                        onTap: onOK == null ? (){
                          Navigator.of(context, rootNavigator: true).pop();
                        } : onOK,
                        child: Container(
                          margin: EdgeInsets.only(right: 0),
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                          child: Text(
                            'ok',
                            style: TextStyle(
                                color: Theme.of(context).accentColor,
                                fontSize: 15,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ) : Container(
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Divider(
                  color: Colors.black26,
                  height: 1,
                ),
                Expanded(
                  child: Container(
                      color: backgroundColor,
                      child: build != null ? ( scrollAble ?CoScroll(onRefresh: (String value) {  },
                      child: build) : build) : SizedBox(width: 0, height: 0,)
                  ),
                ),
              ],
            ),
          ),
        );
      });
}